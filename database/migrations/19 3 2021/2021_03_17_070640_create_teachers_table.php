<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teachers', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('standards_id')->unsigned()->index();
            $table->string('name');
            $table->string('qualification');
            $table->string('mobile_no');
            $table->date('email');
            $table->string('password');
            // $table->string('title');
            $table->timestamps();
        });
         Schema::table('teachers', function (Blueprint $table) {
            $table->foreign('standards_id')->references('id')->on('standards')->onDelete('CASCADE')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teachers');
    }
}
