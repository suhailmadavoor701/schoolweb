<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImagesCSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('images_c_s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('album_c_s_id')->unsigned()->index();
            $table->string('image');
            $table->timestamps();
        });

        Schema::table('album_c_s', function (Blueprint $table) {
            $table->foreign('album_c_s_id')->references('id')->on('album_c_s')->onDelete('CASCADE')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images_c_s');
    }
}
