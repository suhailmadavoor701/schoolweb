<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImages1sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('images1', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('album1_id')->unsigned()->index();
            $table->string('image');
            $table->timestamps();
        });

        Schema::table('images1', function (Blueprint $table) {
            $table->foreign('album1_id')->references('id')->on('album1s')->onDelete('CASCADE')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images1s');
    }
}
