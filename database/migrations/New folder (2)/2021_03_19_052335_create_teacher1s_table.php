<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeacher1sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teacher1s', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('standard_id')->index()->unsigned();
            $table->string('name');
            $table->string('qualification');
            $table->date('mobile_no');
            $table->string('email');
            $table->string('password');
            $table->timestamps();
            $table->foreign('standard_id')->references('id')->on('standard1s')->onDelete('CASCADE')->onUpdate('CASCADE');
            
        });
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teacher1s');
    }
}
