<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePmaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pmaterials', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('subject_id')->index()->unsigned();
            $table->bigInteger('standard_id')->index()->unsigned();
            $table->string('file');
            $table->string('title');
            $table->timestamps();
            $table->foreign('subject_id')->references('id')->on('subjects')->onDelete('CASCADE')->onUpdate('CASCADE');
            $table->foreign('standard_id')->references('id')->on('standard1s')->onDelete('CASCADE')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pmaterials');
    }
}
