<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('subject_id')->index()->unsigned();
            $table->bigInteger('standard_id')->index()->unsigned();
            $table->string('link');
            $table->string('title');
            $table->timestamps();
            $table->foreign('subject_id')->references('id')->on('subjects')->onDelete('CASCADE')->onUpdate('CASCADE');
            $table->foreign('standard_id')->references('id')->on('standard1s')->onDelete('CASCADE')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
