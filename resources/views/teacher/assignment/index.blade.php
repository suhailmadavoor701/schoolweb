@extends('layouts.tables')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        AssignmentTable
        <!-- <small>advanced tables</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        
        
      
        <a href="assignment/create"><button class="open-button" onclick="">Add New Assignment</button></a>
        <a href="submitedlist"><button class="open-button" onclick="">List Of Submitted Assignments</button></a>
       
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Marks</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Subject Name</th>
                  <th>Topic</th>
                  <th>Last Date</th>
                  <th>Action</th>
                  
                </tr>
                </thead>
                
                
               <tbody>
               @foreach($assignments as $mark)
                <tr>
                  <td>{{$mark->name}}</td>
                  <td>{{$mark->topic}}
                  </td>
                  <td>{{$mark->date}}</td>
                  <td>
                                          <form action="{{action('assignmentController@destroy',$mark->id)}}" method="post">
                                            {{csrf_field()}}
                                            <input name="_method" type="hidden" value="DELETE">
                                           <button onclick="if (confirm('Are you sure..?')) commentDelete(1); return false" class="btn btn-warning"  type="submit">Delete</button>
                                          </form>

                                        </td>
                  
                </tr>

                @endforeach()
                </tbody>
               
                
              
            
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

         
  

@endsection