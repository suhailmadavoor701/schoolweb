@extends('layouts.tables')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pdf
        <!-- <small>advanced tables</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Pdf</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


          @if(Session::has('status'))
<p class="alert alert-info">{{ Session::get('status') }}</p>
@endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        
        <a href="pmaterials/create"><button class="open-button" onclick="">Add New Materials</button></a>
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Pdf Materails</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Subject Name</th>
                  <th>Title</th>
                  <th>Pdf</th>
                  <th>Action</th>
                  
                </tr>
                </thead>
                
                
               <tbody>
               @foreach($pmaterials as $mark)
                <tr>
                  <td>{{$mark->name}}</td>
                  <td>{{$mark->title}}
                  </td>
                  <td><a target="_blank" href="{{ asset('uploads/pdf/'.$mark->file) }}">{{$mark->file}}</a></td>
                  <!-- <td>{{$mark->file}}</td> -->
                  <td>
                                          <form action="{{action('pmaterialController@destroy',$mark->id)}}" method="post">
                                            {{csrf_field()}}
                                            <input name="_method" type="hidden" value="DELETE">
                                           <button onclick="if (confirm('Are you sure..?')) commentDelete(1); return false" class="btn btn-warning"  type="submit">Delete</button>
                                          </form>

                                        </td>
                  
                </tr>

                @endforeach()
                </tbody>
               
                
              
            
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

         
  

@endsection