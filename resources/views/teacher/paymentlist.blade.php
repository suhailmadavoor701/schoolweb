@extends('layouts.tables')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Student Table
        <!-- <small>advanced tables</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
         
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Marks</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Student Name</th>
                  <th>Roll No</th>
                  <th>Class </th>
                  <th>Section</th>
                  <th>Amount</th>
                  
                </tr>
                </thead>
                
                
               <tbody>
               @foreach($list as $mark)
                <tr>
                  <td>{{$mark->name}}</td>
                  <td>{{$mark->no}}
                  </td>
                  <td>{{$mark->class}}</td>
                  <td>{{$mark->section}}</td>
                  <td>{{$mark->amount}}</td>

                </tr>

                @endforeach()
                </tbody>
               
               
                
              
            
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

         
  

@endsection