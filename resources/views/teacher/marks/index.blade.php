@extends('layouts.tables')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Marks Table
        <!-- <small>advanced tables</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
         
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Marks</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Subject Name</th>
                  <th>Roll No</th>
                  <th>Marks</th>
                  <th>Action</th>
                  
                </tr>
                </thead>
                
                
               <tbody>
               @foreach($marks as $mark)
                <tr>
                  <td>{{$mark->name}}</td>
                  <td>{{$mark->no}}
                  </td>
                  <td>{{$mark->mark}}</td>
                  <td>
                                          <form action="{{action('markController@edit',$mark->id)}}" method="get">
                                            {{csrf_field()}}
                                            <input name="_method" type="hidden" value="DELETE">
                                           <button onclick="if (confirm('Are you sure..?')) commentDelete(1); return false" class="btn btn-warning"  type="submit">Edit</button>
                                          </form>

                                        </td>
                  
                </tr>

                @endforeach()
                </tbody>
               
                
              
            
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

         
  

@endsection