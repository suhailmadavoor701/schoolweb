@extends('layouts.teacher')

<br>
@section('content')


    @section('content')


    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


          @if(Session::has('status'))
<p class="alert alert-info">{{ Session::get('status') }}</p>
@endif
 <section class="content-header">
      <h1>
        Mark Uploading
        {{-- <small>preview of simple tables</small> --}}
      </h1>
      <ol class="breadcrumb">
        <li><a href="/home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Mark</a></li>
        <li class="active">Create</li>
      </ol>
    </section>

   <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-6">

        <div class="box box-primary">
            <div class="box-header with-border">
              {{-- <h3 class="box-title">Banner Uploading</h3> --}}
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{ url('marks')}}" method="post" enctype="multipart/form-data">
            	{{csrf_field()}}
              <div class="box-body">
              <div class="form-group">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Select Subject</label>
                                    <select class="form-control" name="subject_id">
                                      <option class="form-control" >Select The Subject</option>
                                      @foreach($table as $item)



                                        <option required="" name="subject_id"  class="form-control" value="{{$item->id}}">{{$item->name}}</option>
                                      @endforeach
                                 </select>
                             </div>
                </div>


                <div class="form-group">
                  <label for="exampleInputFile">File input</label>
                  <input type="file" name="image" required="" id="exampleInputFile">
                    <a target="_blank" href="{{ URL::asset('users/img/csvdemo.csv') }}" class="help-block">Demo Csv Download From here</a>

                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>

          </div>
      </div>
  </div>
</section>

@endsection
