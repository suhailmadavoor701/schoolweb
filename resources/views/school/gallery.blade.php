@extends('layouts.school')
@section('content')


       <!-- Main content Start -->
        <div class="main-content">
            <!-- Breadcrumbs Start -->
            <div class="rs-breadcrumbs breadcrumbs-overlay">
                <div class="breadcrumbs-img">
                    <img src="{{URL::asset('users/images/breadcrumbs/4.jpg')}}" alt="Breadcrumbs Image">
                </div>
                <div class="breadcrumbs-text white-color">
                    <h1 class="page-title">Gallery</h1>
                    <ul>
                        <li>
                            <a class="active" href="index.html">Educavo</a>
                        </li>
                        <li>Gallery</li>
                    </ul>
                </div>
            </div>
            <!-- Breadcrumbs End -->            

            <!-- Events Section Start -->
            <div class="rs-gallery pt-100 pb-100 md-pt-70 md-pb-70">
                <div class="container">
                   <div class="row">

                   @if($count > 0)

      @foreach($gallery as $gall)
                     <div class="col-lg-4 col-md-6">
                            <div class="gallery-item">
                                <div class="gallery-img">
                                    <img src="uploads/gallery/{{$gall->image}}" alt="">
                                </div>
                                <div class="title">
                                    {{$gall->name}}
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @else
                        <div class="row">
                <div class="col-md-12">
                    <h1 class="text-center">There Are No Photos </h1>
                </div>
            </div>
            @endif
                   
                     
                   </div>
                </div> 
            </div>
            <!-- Events Section End -->  

            

            
        </div> 
        <!-- Main content End --> 
@endsection 