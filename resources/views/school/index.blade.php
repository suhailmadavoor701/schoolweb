@extends('layouts.school')

@section('content')


      <!-- Canvas Menu start -->
      <nav class="right_menu_togle hidden-md">
                    <div class="close-btn">
                        <div id="nav-close">
                            <div class="line">
                                <span class="line1"></span><span class="line2"></span>
                            </div>
                        </div>
                    </div>
                    <div class="canvas-logo">
                        <a href="index.html"><img src="{{URL::asset('users/images/logo-dark.png')}}" alt="logo"></a>
                    </div>
                    <div class="offcanvas-text">
                        <p>We denounce with righteous indige nationality and dislike men who are so beguiled and demo  by the charms of pleasure of the moment data com so blinded by desire.</p>
                    </div>
                    <div class="offcanvas-gallery">
                        <div class="gallery-img">
                            <a class="image-popup" href="{{URL::asset('users/images/gallery/1.jpg')}}"><img src="{{URL::asset('users/images/gallery/1.jpg')}}" alt=""></a>
                        </div>
                        <div class="gallery-img">
                            <a class="image-popup" href="{{URL::asset('users/images/gallery/2.jpg')}}"><img src="{{URL::asset('users/images/gallery/2.jpg')}}" alt=""></a>
                        </div>
                        <div class="gallery-img">
                            <a class="image-popup" href="{{URL::asset('users/images/gallery/3.jpg')}}"><img src="{{URL::asset('users/images/gallery/3.jpg')}}" alt=""></a>
                        </div>
                        <div class="gallery-img">
                            <a class="image-popup" href="{{URL::asset('users/images/gallery/4.jpg')}}"><img src="{{URL::asset('users/images/gallery/4.jpg')}}" alt=""></a>
                        </div>
                        <div class="gallery-img">
                            <a class="image-popup" href="{{URL::asset('users/images/gallery/5.jpg')}}"><img src="{{URL::asset('users/images/gallery/5.jpg')}}" alt=""></a>
                        </div>
                        <div class="gallery-img">
                            <a class="image-popup" href="{{URL::asset('users/images/gallery/6.jpg')}}"><img src="{{URL::asset('users/images/gallery/6.jpg')}}" alt=""></a>
                        </div>
                    </div>
                    <div class="map-img">
                        <img src="{{URL::asset('users/images/map.jpg')}}" alt="">
                    </div>
                    <div class="canvas-contact">
                        <ul class="social">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </nav>
                <!-- Canvas Menu end -->
            </header>
            <!--Header End-->
        </div>
        <!--Full width header End-->

		<!-- Main content Start -->
        <div class="main-content">
            <!-- Banner Section Start -->
            <div  style="background-image: url('users/images/banner/home3.jpg');"  id="rs-banner" class="rs-banner style3">
                <div class="container pt-90 md-pt-50">
                    <div class="banner-content pb-13">
                        <div class="row">
                            <div class="col-lg-7">
                                <h1 class="banner-title white-color wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="3000ms">People Expect to be Bored eLearning.</h1>
                                <div class="banner-desc white-color wow fadeInRight" data-wow-delay="500ms" data-wow-duration="4000ms">Every act of conscious learning requires the willingness to suffer an <br> injury to one’s self-esteem.</div>
                                <ul class="banner-btn wow fadeInUp" data-wow-delay="700ms" data-wow-duration="2000ms">
                                    <li><a class="readon3" href="#">Get Started</a></li>

                                    <!-- <li><a class="readon3 active" href="#">Read More</a></li> -->
                                </ul>
                            </div>
                        </div>
                        <div class="banner-image hidden-md">
                            <div id="stuff">
                                <div data-depth="0.3">
                                    <img src="{{URL::asset('users/images/banner/bnr3-top.png')}}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Banner Section End -->



            <!-- About Section Start -->
            <div id="rs-about" class="rs-about style3 pt-100 md-pt-70">
                <div class="container">
                    <div class="row y-middle">
                        <div class="col-lg-4 lg-pr-0 md-mb-30">
                            <div class="about-intro">
                                <div class="sec-title">
                                    <div class="sub-title primary">About Us</div>
                                    <h2 class="title mb-21">The End Result of All True Learning</h2>
                                    <div class="desc big">The key to success is to appreciate how people learn, understand the thought process that goes into instructional design, what works well, and a range of differen</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8 pl-83 md-pl-15">
                            <div class="row rs-counter couter-area">
                                <div class="col-md-4 sm-mb-30">
                                    <div class="counter-item one">
                                        <img class="count-img" src="{{URL::asset('users/images/about/style3/icons/1.png')}}" alt="">
                                        <h2 class="number rs-count kplus">2</h2>
                                        <h4 class="title mb-0">Students</h4>
                                    </div>
                                </div>
                                <div class="col-md-4 sm-mb-30">
                                    <div class="counter-item two">
                                        <img class="count-img" src="{{URL::asset('users/images/about/style3/icons/2.png')}}" alt="">
                                        <h2 class="number rs-count">3.50</h2>
                                        <h4 class="title mb-0">Average CGPA</h4>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="counter-item three">
                                        <img class="count-img" src="{{URL::asset('users/images/about/style3/icons/3.png')}}" alt="">
                                        <h2 class="number rs-count percent">95</h2>
                                        <h4 class="title mb-0">Graduates</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- About Section End -->

            <!-- Popular Course Section Start -->
            <div class="rs-popular-courses style2 bg3 pt-94 pb-200 md-pt-64 md-pb-90">
                <div class="container">
                    <div class="sec-title mb-60 text-center md-mb-30">
                        <div class="sub-title primary">Top Courses</div>
                        <h2 class="title mb-0">Popular Courses</h2>
                    </div>
                    <div class="row">


                      @foreach($course as $co)
                        <div class="col-lg-4 col-md-6 mb-30">
                            <div class="course-wrap">
                                <div class="front-part">
                                    <div class="img-part">
                                        <img src="uploads/course/{{$co->image}}" alt="">
                                    </div>
                                    <div class="content-part">
                                        <a class="categorie" href="#">{{$co->name}}</a>
                                        <!-- <h4 class="title"><a href="">From Zero to Hero with Advance Nodejs</a></h4> -->
                                    </div>
                                </div>
                                <div class="inner-part">
                                    <div class="content-part">
                                        <!-- <a class="categorie" href="#">Web Development</a> -->
                                        <h4 class="title"><a href="">{{$co->description}}</a></h4>

                                    </div>
                                </div>

                            </div>
                        </div>
                        @endforeach

                    </div>
                    <!-- <div class="btn-part text-center mt-30">
                        <a class="readon3 dark-hov" href="#">View All Courses</a>
                    </div> -->
                </div>
            </div>
            <!-- Popular Course Section End -->

            <!-- Testimonial Section Start -->
            <div class="rs-testimonial style3">
                <div class="container">
                    <div class="sec-title mb-60 text-center md-mb-30">
                        <div class="sub-title primary">Student Reviews</div>
                        <h2 class="title mb-0">What Our Students Says</h2>
                    </div>
                    <div class="rs-carousel owl-carousel" data-loop="true" data-items="2" data-margin="30" data-autoplay="true" data-hoverpause="true" data-autoplay-timeout="5000" data-smart-speed="800" data-dots="true" data-nav="false" data-nav-speed="false" data-center-mode="false" data-mobile-device="1" data-mobile-device-nav="false" data-mobile-device-dots="false" data-ipad-device="2" data-ipad-device-nav="false" data-ipad-device-dots="false" data-ipad-device2="1" data-ipad-device-nav2="false" data-ipad-device-dots2="false" data-md-device="2" data-md-device-nav="false" data-md-device-dots="true">

                    @foreach($says as $say)
                        <div class="testi-item">
                            <div class="row y-middle no-gutter">
                                <div class="col-md-4">
                                    <div class="user-info">
                                        <!-- <img src="{{URL::asset('users/images/testimonial/style3/1.png')}}" alt=""> -->
                                        <h4 class="name">{{$say->name}}</h4>
                                        <span class="designation">{{$say->designation}}</span>

                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="desc">{{$say->words}}</div>
                                </div>
                            </div>
                        </div>

                        @endforeach


                    </div>
                </div>
            </div>
            <!-- Testimonial Section End -->

            <!-- Blog Section Start -->
            <div id="rs-blog" class="rs-blog style1 modify1 pt-85 pb-100 md-pt-70 md-pb-70">
                <div class="container">
                    <div class="sec-title mb-60 md-mb-30 text-center">
                        <div class="sub-title primary"> </div>
                        <h2 class="title mb-0">Latest Notifications & Events</h2>
                    </div>
                    <div class="row">
                        <div class="col-lg-7 pr-60 md-pr-15 md-mb-30">

                            @if($count<1)
                            <div class="col-md-6">
                                    <div class="blog-content">
                                        <ul class="blog-meta">
                                            <!-- <li><i class="fa fa-user-o"></i> </li> -->
                                            <!-- <li><i class="fa fa-calendar"></i>June 15, 2019</li> -->
                                        </ul>
                                        <h3 class="title"><a href="">No Events </a></h3>
                                        <div class="btn-part">
                                            <!-- <a class="readon-arrow" href="#">Read More</a> -->
                                        </div>
                                    </div>
                                </div>
                          @elseif($count==1)
                            <div class="row no-gutter white-bg blog-item mb-35">
                                <div class="col-md-6">
                                    <div class="image-part">
                                        <a href="#"><img src="uploads/events/{{$events['0']->image}}" alt=""></a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="blog-content">
                                        <ul class="blog-meta">
                                            <li><i class="fa fa-user-o"></i> {{$events['0']->name}}</li>
                                            <!-- <li><i class="fa fa-calendar"></i>June 15, 2019</li> -->
                                        </ul>
                                        <h3 class="title"><a href="">{{$events['0']->description}}</a></h3>
                                        <div class="btn-part">
                                            <!-- <a class="readon-arrow" href="#">Read More</a> -->
                                        </div>
                                    </div>
                                </div>
                            </div>

                          @else


                          <div class="row no-gutter white-bg blog-item mb-35">
                                <div class="col-md-6">
                                    <div class="image-part">
                                        <a href="#"><img src="uploads/events/{{$events['0']->image}}" alt=""></a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="blog-content">
                                        <ul class="blog-meta">
                                            <li><i class="fa fa-user-o"></i> {{$events['0']->name}}</li>
                                            <!-- <li><i class="fa fa-calendar"></i>June 15, 2019</li> -->
                                        </ul>
                                        <h3 class="title"><a href="">{{$events['0']->description}}</a></h3>
                                        <div class="btn-part">
                                            <!-- <a class="readon-arrow" href="#">Read More</a> -->
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row no-gutter white-bg blog-item mb-35">
                                <div class="col-md-6">
                                    <div class="image-part">
                                        <a href="#"><img src="uploads/events/{{$events['1']->image}}" alt=""></a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="blog-content">
                                        <ul class="blog-meta">
                                            <li><i class="fa fa-user-o"></i> {{$events['1']->name}}</li>
                                            <!-- <li><i class="fa fa-calendar"></i>June 15, 2019</li> -->
                                        </ul>
                                        <h3 class="title"><a href="">{{$events['1']->description}}</a></h3>
                                        <div class="btn-part">
                                            <!-- <a class="readon-arrow" href="#">Read More</a> -->
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @endif


                        </div>
                        <div class="col-lg-5 lg-pl-0">

                          @if($webcount<1)
                          <div class="content-part">
                                    <div class="categorie">
                                        <a href="#"></a> <a href="#"></a>
                                    </div>
                                    <h4 class="title mb-0"><a href="">No Notifications</a></h4>
                                </div>

                            @elseif($webcount==1)

                            <div class="events-short mb-28">

                                <div class="content-part">
                                    <div class="categorie">
                                        <a href="#"></a> <a href="#">{{$webnoti['0']->section}}</a>
                                    </div>
                                    <h4 class="title mb-0"><a href="">{{$webnoti['0']->description}}</a></h4>
                                </div>
                            </div>
                            @else
                            <div class="events-short mb-28">

                                <div class="content-part">
                                    <div class="categorie">
                                        <a href="#"></a> <a href="#">{{$webnoti['0']->section}}</a>
                                    </div>
                                    <h4 class="title mb-0"><a href="">{{$webnoti['0']->description}}</a></h4>
                                </div>
                            </div>
                            <div class="events-short mb-28">

                                <div class="content-part">
                                    <div class="categorie">
                                        <a href="#"></a> <a href="#">{{$webnoti['1']->section}}</a>
                                    </div>
                                    <h4 class="title mb-0"><a href="">{{$webnoti['1']->description}}</a></h4>
                                </div>
                            </div>
                            <div class="events-short mb-28">

                                <div class="content-part">
                                    <div class="categorie">
                                        <a href="#"></a> <a href="#">{{$webnoti['2']->section}}</a>
                                    </div>
                                    <h4 class="title mb-0"><a href="">{{$webnoti['2']->description}}</a></h4>
                                </div>
                            </div>
                            <div class="events-short mb-28">

                                <div class="content-part">
                                    <div class="categorie">
                                        <a href="#"></a> <a href="#">{{$webnoti['3']->section}}</a>
                                    </div>
                                    <h4 class="title mb-0"><a href="">{{$webnoti['3']->description}}</a></h4>
                                </div>
                            </div>
                         @endif


                        </div>
                    </div>
                </div>
            </div>
            <!-- Blog Section End -->


        </div>
        <!-- Main content End -->

        @endsection
