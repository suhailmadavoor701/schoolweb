@extends('layouts.school')

@section('content')

    

		<!-- Main content Start -->
        <div class="main-content">
            <!-- Breadcrumbs Start -->
            <div class="rs-breadcrumbs breadcrumbs-overlay">
                <div class="breadcrumbs-img">
                    <img src="{{URL::asset('users/images/breadcrumbs/2.jpg')}}" alt="Breadcrumbs Image">
                </div>
              
            </div>
            <!-- Breadcrumbs End -->            

    		<!-- Register Section -->
            <section class="register-section pt-100 pb-100">
                <div class="container">
                    <div class="register-box">
                        
                        <div class="sec-title text-center mb-30">
                            <h2 class="title mb-10">Register</h2>
                        </div>
     
                        <!-- Login Form -->
                                                @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if(Session::has('status'))
                            <p class="alert alert-info">{{ Session::get('status') }}</p>
                            @endif
                            
                            @if(Session::has('danger'))
                            <p class="alert alert-info">{{ Session::get('danger') }}</p>
                            @endif
                            

                        <div class="styled-form">
                            <div id="form-messages"></div>
                            <form  method="post" action="/registerstudent">   
                            {{csrf_field()}}                            
                                <div class="row clearfix">                                    
                                    <!-- Form Group -->
                                    <div class="form-group col-lg-12 mb-25">
                                        <input type="text"  name="name" value="" placeholder="Name" required>
                                    </div>
                                    
                                    <!-- Form Group -->
                                   
                                    
                                    <!-- Form Group -->
                                    <div class="form-group col-lg-12">
                                        <input type="email"  name="email" value="" placeholder="Email address " required>
                                    </div>
                                    <div class="form-group col-lg-12">
                                            <label for="exampleInputEmail1">Select Course</label>
                                                        <select class="form-control" name="standard_id">
                                                        <option class="form-control" >Select The Course</option>
                                                        @foreach($table as $item)



                                                            <option required="" name="standard_id"  class="form-control" value="{{$item->id}}">{{$item->standard}}</option>
                                                        @endforeach
                                                    </select>
                                            
                                    </div>
                                    
                                    <!-- Form Group -->
                                    <div class="form-group col-lg-12">
                                        <input type="no"  name="roll_no" value="" placeholder="Class Roll Number" required>
                                    </div>   
                                    <div class="form-group col-lg-12">
                                        <input type="text"  name="mobile_no" value="" placeholder="Phone Number" required>
                                    </div>    
                                    <!-- Form Group -->
                                    <div class="form-group col-lg-12">
                                        <input type="text" name="password" value="" placeholder="Password" required>
                                    </div>    
                                    <!-- Form Group -->
                                    <div class="form-group col-lg-12 col-md-12 col-sm-12 text-center">
                                        <button type="submit" class="readon register-btn"><span class="txt">Register</span></button>
                                    </div>
                                 
                                   
                                    <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                        <div class="users">Already have an account? <a href="login.html">Sign In</a></div>
                                    </div>
                                    
                                </div>
                                
                            </form>
                        </div>
                        
                    </div>
                </div>
            </section>
            <!-- End Login Section --> 

        </div> 
@endsection