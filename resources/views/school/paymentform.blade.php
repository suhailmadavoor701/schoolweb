@extends('layouts.school')

@section('content')

		<!-- Main content Start -->
        <div class="main-content">
            <!-- Breadcrumbs Start -->
            <div class="rs-breadcrumbs breadcrumbs-overlay">
                <div class="breadcrumbs-img">
                    <img src="{{URL::asset('users/images/breadcrumbs/2.jpg')}}" alt="Breadcrumbs Image">
                </div>
                <div class="breadcrumbs-text white-color">
                    <h1 class="page-title">My Account</h1>
                    <ul>
                        <li>
                            <a class="active" href="index.html">Home</a>
                        </li>
                        <li>My Account</li>
                    </ul>
                </div>
            </div>
            <!-- Breadcrumbs End -->            

    		<!-- My Account Section Start -->
           
                     
    		<div class="rs-login pt-100 pb-100 md-pt-70 md-pb-70">
                <div class="container">
                    <div class="noticed">
                        <div class="main-part">                           
                        <div class="method-account">
                                <h2 class="login">Payment</h2>
                                <form action="/payment" method="post">
                                {{csrf_field()}}  
                                @if(Session::has('status'))
                            <p class="alert alert-info">{{ Session::get('status') }}</p>
                            @endif
                            
                            @if(Session::has('danger'))
                            <p class="alert alert-info">{{ Session::get('danger') }}</p>
                            @endif
                                    <input type="text" name="name" placeholder="Name" required="">
                                    <input type="text" name="class" placeholder="Class" required="">
                                    <input type="text" name="no" placeholder="Roll No" required="">
                                    <input type="text" name="section" placeholder="For Which Section You Paying" required="">
                                    <input type="number" name="amount" placeholder="Amount" required="">
                                    <button type="submit" class="readon submit-btn">Submit</button>
                                
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- My Account Section End -->  

        </div> 
        <!-- Main content End --> 


        @if(Session::has('data'))
 
 <div class="container tex-center mx-auto">
 <form action="/pay" method="post" class="text-center mx-auto mt-5">
   <script
       src="https://checkout.razorpay.com/v1/checkout.js"
       data-key="rzp_test_NjLf3nMNeudyaD"
 data-amount="{{Session::get('data.amount')}}" 
       data-currency="INR"
 data-order_id="{{Session::get('data.order_id')}}"
       data-buttontext="Pay with Razorpay"
       data-name="Coffee"
       data-description="Test transaction"
      
       data-theme.color="#F37254"
   ></script>
   <input type="hidden" custom="Hidden Element" name="hidden">
   </form>

 </div>
 
 @endif
@endsection