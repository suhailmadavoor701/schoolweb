@extends('layouts.school')
@section('content')
<div style="background-color: rgb(253, 253, 253);" class="bg2">
    <!-- Blog Section Start -->
    <div id="rs-blog" class="rs-blog style1 pt-94 pb-100 md-pt-64 md-pb-70">
        <div class="container">
            <div class="sec-title mb-60 md-mb-30 text-center">
                <br>
                <br>
                <br>
                {{-- <div class="sub-title primary">News Update </div> --}}
                <h2 class="title mb-0">Latest Notifications</h2>
            </div>
            <div class="row">

                <div class="col-lg-10 lg-pl-0">
                    @foreach ($notify as $not )
                    <div class="events-short mb-30 wow fadeInUp" data-wow-delay="300ms" data-wow-duration="2000ms">
                        <div class="date-part">
                            <span class="month">{{$not->date}}</span>
                            {{-- <div class="date">20</div> --}}
                        </div>
                        <div class="content-part">
                            <div class="categorie">
                                <a href="#"></a> <a target="_blank" href="{{$not->link}}">PDF DOWNLOAD FROM HERE</a>

                            </div>
                            <p><a href="#"></a> <a target="_blank" href="">{{$not->content}}</a></p>

                            <h4 class="title mb-0"><a href="#">{{$not->tag}}</a></h4>

                        </div>
                    </div>

                    @endforeach


                </div>
            </div>
        </div>
    </div>
    <!-- Blog Section End -->


</div>
 @endsection
