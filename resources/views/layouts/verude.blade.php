<!doctype html>
<html lang="en-US">
	

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>

		<!-- Meta -->
		<meta charset="UTF-8">
		<title>JB GROUP</title>
		<meta name="keywords" content="Rayleigh, HTML5 Template, Minimalistic, Elegant, Creative, Clean">
		<meta name="description" content="Rayleigh - Minimalistic and Elegant HTML Template">
		<meta name="author" content="R6 Themes">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Favicons -->
		<!-- <link rel="shortcut icon" href="img/favicons/favicon.png"> -->
		<link rel="apple-touch-icon" href="{{URL::asset('users/img/favicons/apple-touch-icon.png')}}">
		<link rel="apple-touch-icon" sizes="72x72" href="{{URL::asset('users/img/favicons/apple-touch-icon-72x72.png')}}">
		<link rel="apple-touch-icon" sizes="114x114" href="{{URL::asset('users/img/favicons/apple-touch-icon-114x114.png')}}">

		<!-- CSS -->
		<link rel="stylesheet" href="{{URL::asset('users/css/reset.css')}}">
		<link rel="stylesheet" href="{{URL::asset('users/css/bootstrap.min.css')}}">
		<link rel="stylesheet" href="{{URL::asset('users/css/font-awesome.min.css')}}">
		<link href="http://fonts.googleapis.com/css?family=Raleway:300|Muli:300" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="{{URL::asset('users/css/idangerous.swiper.css')}}">
		<link rel="stylesheet" href="{{URL::asset('users/css/style.css')}}">

	</head>

	<body>

		<!-- Preloader -->
		<div id="preloader">
			<div id="status"></div>
		</div>

		<!-- Overlay -->
		<div id="overlay"></div>

		<!-- Top header -->
		<div id="top">

			<!-- Sidebar button -->
			<a href="#" id="sidebar-button"></a>

			<!-- Logo -->
			<header id="logo">
				<a href="/" title=""><img src="{{URL::asset('users/img/logo.png')}}" alt="jb"></a> 
			</header>

			<!-- Fullscreen slider navigation arrows -->
			<nav id="nav-arrows">
				<a href="#" class="nav-left hidden"></a>
				<a href="#" class="nav-right"></a>
			</nav>

		</div>

		@yield('content')



		<!-- Collapsible sidebar -->
			<div id="sidebar" style="background-color:black">

				<!-- Widget Area -->
				<div id="widgets">

					<!-- Main menu -->
					<nav id="mainmenu">
						<ul>
							<li><a href="/" class="">Home</a></li>
							<li><a href="/services">Who we are</a></li>
							<li><a href="jabir">Jabar Bin Ahmmed</a></li>
							<li><a href="/we">We</a></li>
							{{-- <li><a href="#">Services</a></li> --}}
							<li><a target="_blank" href="{{ URL::asset('users/img/portfolio.pdf') }}">Portfolio</a></li>
						
							<li><a href="/bloguser">Blog</a></li>
							<li><a href="teamuser">Team</a></li>
							<li><a href="testimonial">Testimonial</a></li>
							<li>
								<a href="#">Projects</a>
								<ul>
									<li><a href="/comercial">Commercial </a></li>
									<li><a href="/residentail">Residential </a></li>
									
								</ul>
							</li>
							<li><a href="/contact">Contact</a></li>
						</ul>
					</nav>

				</div>

				<!-- Copyright -->
				<footer>
					<p style="color: white" class="copyright"><a style="color: white" target="_blank" href="https://risbuzz.com/	" title=""> © Copyright 2020 Risbuzz</a></p>

				</footer>

			</div>

		

		<!-- JavaScripts -->
		<script type='text/javascript' src="{{URL::asset('users/js/jquery.min.js')}}"></script>
		<script type='text/javascript' src="{{URL::asset('users/js/bootstrap.min.js')}}"></script>
		<script type='text/javascript' src="{{URL::asset('users/js/swiper/idangerous.swiper.min.js')}}"></script>
		<script type='text/javascript' src="{{URL::asset('users/js/masonry/masonry.pkgd.min.js')}}"></script>
		<script type='text/javascript' src="{{URL::asset('users/js/isotope/jquery.isotope.min.js')}}"></script>
		<script type='text/javascript' src="{{URL::asset('users/js/custom.js')}}"></script>

	</body>


</html>
