<!DOCTYPE html>
<html lang="zxx">

<head>
        <!-- meta tag -->
        <meta charset="utf-8">
        <title>ESTUDIO</title>
        <meta name="description" content="">
        <!-- responsive tag -->
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- favicon -->
        <link rel="apple-touch-icon" href="apple-touch-icon.html">
        <link rel="shortcut icon" type="image/x-icon" href="{{URL::asset('users/images/fav.png')}}">
        <!-- Bootstrap v4.4.1 css -->
        <link rel="stylesheet" type="text/css" href="{{URL::asset('users/css/bootstrap.min.css')}}">
        <!-- font-awesome css -->
        <link rel="stylesheet" type="text/css" href="{{URL::asset('users/css/font-awesome.min.css')}}">
        <!-- animate css -->
        <link rel="stylesheet" type="text/css" href="{{URL::asset('users/css/animate.css')}}">
        <!-- owl.carousel css -->
        <link rel="stylesheet" type="text/css" href="{{URL::asset('users/css/owl.carousel.css')}}">
        <!-- slick css -->
        <link rel="stylesheet" type="text/css" href="{{URL::asset('users/css/slick.css')}}">
        <!-- off canvas css -->
        <link rel="stylesheet" type="text/css" href="{{URL::asset('users/css/off-canvas.css')}}">
        <!-- linea-font css -->
        <link rel="stylesheet" type="text/css" href="{{URL::asset('users/fonts/linea-fonts.css')}}">
        <!-- flaticon css  -->
        <link rel="stylesheet" type="text/css" href="{{URL::asset('users/fonts/flaticon.css')}}">
        <!-- magnific popup css -->
        <link rel="stylesheet" type="text/css" href="{{URL::asset('users/css/magnific-popup.css')}}">
        <!-- Main Menu css -->
        <link rel="stylesheet" href="{{URL::asset('users/css/rsmenu-main.css')}}">
        <!-- spacing css -->
        <link rel="stylesheet" type="text/css" href="{{URL::asset('users/css/rs-spacing.css')}}">
        <!-- style css -->
        <link rel="stylesheet" type="text/css" href="{{URL::asset('users/css/style.css')}}"> <!-- This stylesheet dynamically changed from style.less -->
        <!-- responsive css -->
        <link rel="stylesheet" type="text/css" href="{{URL::asset('users/css/responsive.css')}}">
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
    .oness:hover{
   -webkit-box-shadow: none !important;
    box-shadow: none !important;
    }

    #fixedbutton {
    position: fixed;
    bottom:0px;
    right: -124px;
    border:none;
    box-shadow: none !important;
    width: 200px;
    height: 200px;
    /*background-color: green;*/
    color: green;
        z-index: 2;


    }
}
</style>
    </head>
    <body class="home-style3">


 <a id="fixedbutton" target="_blank" href="https://wa.me/917012832207" title=""><i id="fixedbutton"  style="font-size:45px" class="fa fa-whatsapp"></i></a>


        <!--Preloader area start here-->
        <!-- <div id="loader" class="loader">
            <div class="loader-container">
                <div class='loader-icon'>
                    <img src="{{URL::asset('users/images/pre-logo.png')}}" alt="">
                </div>
            </div>
        </div> -->
        <!--Preloader area End here-->

        <!--Full width header Start-->
        <div class="full-width-header header-style2 modify1">
            <!--Header Start-->
            <header id="rs-header" class="rs-header">
                <!-- Topbar Area Start -->
                <div  class="topbar-area d-none">
                    <div class="container">
                        <div class="row y-middle">
                            <div class="col-md-6">
                                <ul class="topbar-contact">
                                    <li>
                                        <i class="flaticon-email"></i>
                                        <a href="mailto:support@rstheme.com">support@rstheme.com</a>
                                    </li>
                                    <li>
                                        <i class="flaticon-call"></i>
                                        <a href="tel:+0885898745">(+088) 589-8745</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-6 text-right">
                                <ul class="topbar-right">
                                    <li class="login-register">
                                        <i class="fa fa-sign-in"></i>
                                        <a href="login.html">Login</a>/<a href="register.html">Register</a>
                                    </li>
                                    <!-- <li class="btn-part">
                                        <a class="apply-btn" href="#">Apply Now</a>
                                    </li> -->
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Topbar Area End -->
                <div class="some">
                    <p style="background-color: red;"></p>
                </div>

                <!-- Menu Start -->
                <div  class="menu-area menu-sticky" >
                    <div class="container-fluid">
                        <div class="row y-middle">
                            <div class="col-lg-4">
                                <div class="logo-cat-wrap">
                                    <div class="logo-part pr-90">
                                        <a href="index.html">
                                            <img src="{{URL::asset('users/images/logo.png')}}" alt="">
                                        </a>
                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-8 text-center">
                                <div class="rs-menu-area">
                                    <div class="main-menu pr-90 md-pr-15">
                                        <div class="mobile-menu">
                                            <a class="rs-menu-toggle">
                                                <i class="fa fa-bars"></i>
                                            </a>
                                        </div>
                                        <nav class="rs-menu">
                                           <ul class="nav-menu">


                                           <li><a href="/">Home</a><li>
                                           <!-- <li><a href="/#rs-about">About Us</a><li> -->
                                           <li><a href="/usergallery">Gallery</a><li>
                                           <li><a href="/contact">Contact</a><li>
                                     <li><a href="/university">University</a><li>

                                           <li><a href="paymentform">Pay My fees</a><li>
                                            <li class="menu-item-has-children">
                                                <a href="#">Student</a>
                                                <ul class="sub-menu">
                                                    <li><a href="studentregistration">Register</a><li>
                                                        <li><a href="studentloginform">Login</a><li>
                                                </ul>
                                            </li>


                                           </ul> <!-- //.nav-menu -->
                                        </nav>
                                    </div> <!-- //.main-menu -->

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Menu End -->
</div>




            @yield('content')
         <!-- Footer Start -->
        <footer id="rs-footer" class="rs-footer">
            <div class="footer-top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-12 col-sm-12 footer-widget md-mb-50">
                            <h4 class="widget-title">Quik Links</h4>
                            <ul class="site-map">
                                <li><a href="#">Home</a></li>
                                <li><a href="#">About us</a></li>
                                <li><a href="#">Gallery</a></li>
                                <li><a href="#">Contact</a></li>

                            </ul>
                        </div>

                        <div class="col-lg-3 col-md-12 col-sm-12 footer-widget">
                            <h4 class="widget-title">Address</h4>
                            <ul class="address-widget">
                                <li>
                                    <i class="flaticon-location"></i>
                                    <div class="desc">503 Old Buffalo Street Northwest #205 New York-3087</div>
                                </li>
                                <li>
                                    <i class="flaticon-call"></i>
                                    <div class="desc">
                                        <a href="tel:(123)-456-7890">(123)-456-7890</a> ,
                                        <a href="tel:(123)-456-7890">(123)-456-7890</a>
                                    </div>
                                </li>
                                <li>
                                    <i class="flaticon-email"></i>
                                    <div class="desc">
                                        <a href="mailto:infoname@gmail.com">infoname@gmail.com</a> ,
                                        <a href="#">www.yourname.com</a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="container">
                    <div class="row y-middle">
                        <div class="col-lg-4 md-mb-20">
                            <div class="footer-logo md-text-center">
                                <a href="index.html"><img src="assets/images/logo.png" alt=""></a>
                            </div>
                        </div>
                        <div class="col-lg-4 md-mb-20">
                            <div class="copyright text-center md-text-left">
                                <p>&copy; 2021 All Rights Reserved. Developed By <a href="">Suhail</a></p>
                            </div>
                        </div>
                        <div class="col-lg-4 text-right md-text-left">
                            <ul class="footer-social">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer End -->

        <!-- start scrollUp  -->
        <div style="padding-right: 13px;" id="scrollUp">
            <i class="fa fa-angle-up"></i>
        </div>
        <!-- End scrollUp  -->

        <!-- Search Modal Start -->
        <div aria-hidden="true" class="modal fade search-modal" role="dialog" tabindex="-1">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span class="flaticon-cross"></span>
            </button>
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="search-block clearfix">
                        <form>
                            <div class="form-group">
                                <input class="form-control" placeholder="Search Here..." type="text">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Search Modal End -->

        <!-- modernizr js -->
        <script src="{{URL::asset('users/js/modernizr-2.8.3.min.js')}}"></script>
        <!-- jquery latest version -->
        <script src="{{URL::asset('users/js/jquery.min.js')}}"></script>
        <!-- Bootstrap v4.4.1 js -->
        <script src="{{URL::asset('users/js/bootstrap.min.js')}}"></script>
        <!-- Menu js -->
        <script src="{{URL::asset('users/js/rsmenu-main.js')}}"></script>
        <!-- op nav js -->
        <script src="{{URL::asset('users/js/jquery.nav.js')}}"></script>
        <!-- owl.carousel js -->
        <script src="{{URL::asset('users/js/owl.carousel.min.js')}}"></script>
        <!-- Slick js -->
        <script src="{{URL::asset('users/js/slick.min.js')}}"></script>
        <!-- isotope.pkgd.min js -->
        <script src="{{URL::asset('users/js/isotope.pkgd.min.js')}}"></script>
        <!-- imagesloaded.pkgd.min js -->
        <script src="{{URL::asset('users/js/imagesloaded.pkgd.min.js')}}"></script>
        <!-- wow js -->
        <script src="{{URL::asset('users/js/wow.min.js')}}"></script>
        <!-- Skill bar js -->
        <script src="{{URL::asset('users/js/skill.bars.jquery.js')}}"></script>
        <script src="{{URL::asset('users/js/jquery.counterup.min.js')}}"></script>
         <!-- counter top js -->
        <script src="{{URL::asset('users/js/waypoints.min.js')}}"></script>
        <!-- video js -->
        <script src="{{URL::asset('users/js/jquery.mb.YTPlayer.min.js')}}"></script>
        <!-- magnific popup js -->
        <script src="{{URL::asset('users/js/jquery.magnific-popup.min.js')}}"></script>
        <!-- parallax-effect js -->
        <script src="{{URL::asset('users/js/parallax-effect.min.js')}}"></script>
        <!-- plugins js -->
        <script src="{{URL::asset('users/js/plugins.js')}}"></script>
        <!-- contact form js -->
        <script src="{{URL::asset('users/js/contact.form.js')}}"></script>
        <!-- main js -->
        <script src="{{URL::asset('users/js/main.js')}}"></script>
    </body>

</html>
