<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Teacher Table</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ URL::asset('admin/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{URL::asset('admin/bower_components/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{URL::asset('admin/bower_components/Ionicons/css/ionicons.min.css')}}">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{URL::asset('admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{URL::asset('admin/dist/css/AdminLTE.min.css')}}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{URL::asset('admin/dist/css/skins/_all-skins.min.css')}}">



  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      {{-- <span class="logo-mini"><b>A</b>LT</span> --}}
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>


    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
            <img src="{{ URL::asset('admin/dist/img/jb.jpeg') }}" class="img-circle" alt="User Image">

        <h3 style="color:white">{{Auth::guard('teacher')->user()->name}}</h3>
          <a href="teacherdashborad"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
        <div class="pull-left info">
          <!-- <p>Teacher</p> -->
          <!-- <a href="#"><i class="fa fa-circle text-success"></i> Online</a> -->
        </div>
      </div>

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header"><a href="/teacherdashboard">MAIN NAVIGATION</a></li>
        <li><a href="/payedlist"><i class="fa fa-circle-o"></i>Payments</a></li>
        <li><a href="/subjects"><i class="fa fa-circle-o"></i>Subject</a></li>
        <li><a href="/teacherlist"><i class="fa fa-circle-o"></i>Teachers</a></li>
        <li><a href="/studentlist"><i class="fa fa-circle-o"></i>Students</a></li>
        <li><a href="/notifications"><i class="fa fa-circle-o"></i>Notifications</a></li>
        {{-- <li><a href="/marks"><i class="fa fa-circle-o"></i>Marks</a></li>
        <li><a href="/marks/create/"><i class="fa fa-circle-o"></i>Marks Upload</a></li> --}}
        <li class="treeview">
            <a href="#">
              <i class="fa fa-circle-o"></i> <span>Marks</span>
              <span class="pull-right-container">
                <i class=""></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="/marks"><i class="fa fa-circle-o"></i>marks</a></li>
              <li><a href="/marks/create/"><i class="fa fa-circle-o"></i>Upload Marks</a></li>




            </ul>
          </li>



        <li><a href="/assignment"><i class="fa fa-circle-o"></i>Assignments</a></li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Study Materials</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/pmaterials"><i class="fa fa-circle-o"></i>Pdf Materials</a></li>
            <li><a href="/videos"><i class="fa fa-circle-o"></i>Videos</a></li>
            <li><a href="/livevideos"><i class="fa fa-circle-o"></i>Live Videos</a></li>

          </ul>
        </li>
        <li>

                                <a class="" href="{{ route('logout') }}"
                                     onclick="event.preventDefault();
                                                   document.getElementById('logout-form').submit();">
                                      {{ __('Logout') }}
                                  </a>

                                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                      @csrf
                                  </form>

                  </li>


  </aside>

  @yield('content')

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      {{-- <b>Version</b> 2.4.0 --}}
    </div>
    <strong>Copyright &copy; 2021 <a target="_blank"  href="https://risbuzz.com/">Me</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{URL::asset('admin/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{URL::asset('admin/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- DataTables -->
<script src="{{URL::asset('admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{URL::asset('admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{URL::asset('admin/bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{URL::asset('admin/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{URL::asset('admin/dist/js/demo.js')}}"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>

</html>
