@extends('layouts.admin')

<br>
@section('content')


    @section('content')


    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


          @if(Session::has('status'))
<p class="alert alert-info">{{ Session::get('status') }}</p>
@endif

 <section class="content-header">
      <h1>
        Teacher Uploading
        {{-- <small>preview of simple tables</small> --}}
      </h1>
      <ol class="breadcrumb">
        <li><a href="\home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Teacher</a></li>
        <li class="active">Create</li>
      </ol>
    </section>
 
   <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">

        <div class="box box-primary">
            <div class="box-header with-border">
              {{-- <h3 class="box-title">Banner Uploading</h3> --}}
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="/teacherindex" method="post" enctype="multipart/form-data">
            	{{csrf_field()}}
              <div class="box-body">
                <div class="form-group">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Select Standard</label>
                                    <select class="form-control" name="standard">
                                      <option class="form-control" >Select The Standard</option>
                                      @foreach($table as $item)



                                        <option required="" name="standard"  class="form-control" value="{{$item->id}}">{{$item->standard}}</option>
                                      @endforeach
                                 </select>
                             </div>
                </div>
                <div class="form-group">
                  <label>Name of Teacher</label>
                  <input type="text" name="name" required="" class="form-control" placeholder="Enter name">
                </div>
                <div class="form-group">
                  <label>Qualification</label>
                  <input type="text" name="qualification" required="" class="form-control" placeholder="Enter Qualification">
                </div>
                <div class="form-group">
                  <label>Email</label>
                  <input type="email" name="email" required="" class="form-control" placeholder="Email">
                </div>
                <div class="form-group">
                  <label>Phone No</label>
                  <input type="no" name="phoneno" required="" class="form-control" placeholder="Phone No">
                </div>
                <div class="form-group">
                  <label>Password</label>
                  <input type="password" name="password" required="" class="form-control" placeholder="Password">
                </div>
                
             
                 
              
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
      </div>
  </div>
</section>

@endsection