@extends('layouts.admin')

<br>
@section('content')


    @section('content')


    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


          @if(Session::has('status'))
<p class="alert alert-info">{{ Session::get('status') }}</p>
@endif

 <section class="content-header">
      <h1>
        Blog Editing
        {{-- <small>preview of simple tables</small> --}}
      </h1>
      <ol class="breadcrumb">
        <li><a href="\home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Blog</a></li>
        <li class="active">Editing</li>
      </ol>
    </section>
 
   <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-6">

        <div class="box box-primary">
            <div class="box-header with-border">
              {{-- <h3 class="box-title">Banner Uploading</h3> --}}
            </div>
            <!-- /.box-header -->
            <!-- form start -->
          

    <form method="post" action="{{action('blogController@update', $id)}}" enctype="multipart/form-data">
              {{csrf_field()}}
               <input name="_method" type="hidden" value="PATCH">
              <div class="box-body">
                <div class="form-group">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Select Category</label>
                                    <select class="form-control" name="name">
                                      <option class="form-control" >Select The Category</option>
                                      @foreach($table as $item)




                                      <option value="{{ $item->id }}" selected>{{ $item->name }} </option> 

                                        {{-- <option required="" name="name"  class="form-control" value="{{$item->id}}">{{$item->name}}</option> --}}
                                      @endforeach
                                 </select>
                             </div>
                </div>
                <div class="form-group">
                  <label>Name of Auther</label>
                  <input type="text" name="auther" required="" value="{{ $banner->name }}" class="form-control" placeholder="Enter name">
                </div>
                <div class="form-group">
                  <label>Title</label>
                  <input type="text" name="title" required="" value="{{ $banner->title }}" class="form-control" placeholder="Enter title">
                </div>
                <div class="form-group">
                  <label>Date of bloging</label>
                  <input type="date" name="date" required="" value="{{ $banner->date }}" class="form-control" placeholder="Enter date">
                </div>
                <div class="form-group">
                  <label>Textarea</label>
                  <textarea class="form-control" name="description" value="{{ $banner->description }}"  required="" rows="3" 
                  placeholder="Enter description">{{ $banner->description }}</textarea>
                </div>
             
                <div class="form-group">
                  <label  for="exampleInputFile">File input</label>
                  <input type="file" required="" name="image" class="form-control" value="" >
                                        <label></label><br><br> 
                </div>
              
              
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
      </div>
  </div>
</section>

@endsection





