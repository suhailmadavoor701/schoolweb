@extends('layouts.admin')


@section('content')
   @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


    @section('content')
          @if(Session::has('status'))
<p class="alert alert-info">{{ Session::get('status') }}</p>
@endif

 <section class="content-header">
      <h1>
        Teachers List
        <button><a href="/teacherindex/create">Add New Teacher</a></button>
        {{-- <small>preview of simple tables</small> --}}
      </h1>
      <ol class="breadcrumb">
        <li><a href="/home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Teachers</a></li>
        <!-- <li class="active">Create</li> -->
      </ol>
    </section>




    </section>
     <div class="row">
       <div style="padding-right: 10px;padding-left: 10px;">
        <div class="col-xs-12">
          <div class="box">

            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>

                  <th>Name</th>
                  <th>Standard</th>
                  <th>Email</th>
                  <th>Qualification</th>
                  <th>phoneno</th>
                  <!-- <th>Password</th> -->

                  <!-- <th>Image</th> -->
                  <th>Action</th>
                </tr>
                @foreach($banner as $ban)
                 <tr>

                  <td>{{ $ban->name }}</td>
                  <td>{{ $ban->stdname }}</td>
                  <td>{{ $ban->email }}</td>
                  <td>{{ $ban->qualification }}</td>
                   <td>{{ $ban->mobile_no}}</td>



                                    <td>

                                          <form action="{{action('teacheraddController@destroy',$ban->id)}}" method="post">
                                            {{csrf_field()}}
                                            <input name="_method" type="hidden" value="DELETE">
                                           <button onclick="if (confirm('Are you sure..?')) commentDelete(1); return false" class="btn btn-warning"  type="submit">Delete</button>
                                          </form>

                                        </td>
                </tr>
                @endforeach
              </table>
            </div>

          </div>
        </div>
      </div>
    </div>

        </div>
      </div>

@endsection





