@extends('layouts.student')
@section('content')
<br>


@if($count <1)
<div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-star-o"></i></span>
            

            <div class="info-box-content">
            <h2>No Marks Scored</h2>
           
               
            
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
@else
@foreach($mark as $sub)
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-star-o"></i></span>
            

            <div class="info-box-content">
            <h2>{{$sub->subject}}</h2>
           
               <h2>{{$sub->mark}}</h2>
            
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        @endforeach()
      @endif


@endsection