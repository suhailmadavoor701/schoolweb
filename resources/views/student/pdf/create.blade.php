@extends('layouts.student')
@section('content')
 <section class="content-header">
      <h1>
        Assignment Uploading
        {{-- <small>preview of simple tables</small> --}}
      </h1>
      <ol class="breadcrumb">
        <li><a href="/home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Assignment Maker</a></li>
        <li class="active">Create</li>
      </ol>
    </section>
 
   <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">

        <div class="box box-primary">
            <div class="box-header with-border">
              {{-- <h3 class="box-title">Banner Uploading</h3> --}}
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{ url('pdf')}}" method="post" enctype="multipart/form-data">
            	{{csrf_field()}}
              <div class="box-body">
              <div class="form-group">
              <div class="form-group">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="name" class="form-control" required="" name="name" placeholder=" Name">
                </div>
                <div class="form-group">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Select Subject</label>
                                    <select class="form-control" name="subject_id">
                                      <option class="form-control" >Select The Subject</option>
                                      @foreach($table as $item)



                                        <option required="" name="subject_id"  class="form-control" value="{{$item->id}}">{{$item->name}}</option>
                                      @endforeach
                                 </select>
                             </div>
                </div>
                <!-- <div class="form-group">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Select Class</label>
                                    <select class="form-control" name="standard_id">
                                      <option class="form-control" >Select The Class</option>
                                      @foreach($standtable as $item)



                                        <option required="" name="standard_id"  class="form-control" value="{{$item->id}}">{{$item->standard}}</option>
                                      @endforeach
                                 </select>
                             </div>
                </div> -->

                <div class="form-group">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Select Topic</label>
                                    <select class="form-control" name="topic">
                                      <option class="form-control" >Select The Topic</option>
                                      @foreach($assignment as $item)



                                        <option required="" name="topic"  class="form-control" value="{{$item->id}}">{{$item->topic}}</option>
                                      @endforeach
                                 </select>
                             </div>
                </div>
                
        
                <textarea  name="content" placeholder="Place Content Of the Assignment Here"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>

                </div>
               
             
              
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
           
          </div>
      </div>
  </div>
</section>

@endsection