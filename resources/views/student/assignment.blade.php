@extends('layouts.studenttable')
@section('content')



<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Assignment Table
        <!-- <small>advanced tables</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

        <!-- <a href="assignement/create"><button class="open-button" onclick="">Add New Assignment</button></a> -->
        <div class="box">
            <div class="box-header">
              <h3 class="box-title"></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
              <!-- <h6><a href="/pdfadd">PDF Maker</a></h6> -->
                <thead>
                <tr>

                  <th>Topic</th>
                  <th>Last Date</th>
                  <th >Action</th>

                </tr>
                </thead>


               <tbody>
               @foreach($test1 as $mark)
                <tr>

                  <td>{{$mark->topic}}
                  </td>
                  <td>{{$mark->date}}</td>
                  <td>Not Completed</td>
                  <td><a href="/pdfadd">Upload</a></td>

                </tr>

                @endforeach()


                </tbody>
                <tbody>
               @foreach($ass as $mark)
                <tr>

                  <td>{{$mark->topic}}
                  </td>
                  <td>{{$mark->created_at}}</td>
                  <td>Completed</td>
                  <!-- <td>{{$mark->pdf}}</td> -->
                  <td><a target="_blank" href="/uploads/pdf/{{$mark->pdf}}">View</a></td>
                  <td><a href="/pdfdelete/{{$mark->id}}">Delete</a></td>
                </tr>

                @endforeach()
                </tbody>



                </tfoot>
              </table>
            </div>
        </div>




          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>





@endsection
