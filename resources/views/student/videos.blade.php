@extends('layouts.studenttable')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Videos Table
        <!-- <small>advanced tables</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

         @if($count<1)
                    
                    <div >
                       <h6>No Materials found</h6>
                    </div>
                    
            @else
        
        <!-- <a href="assignement/create"><button class="open-button" onclick="">Add New Assignment</button></a> -->
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Videos</h3>
            </div>
            <!-- /.box-header -->
           
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  
                  <th>Title</th>
                  <th>Pdf</th>
                 
                  
                </tr>
                </thead>
                
                
               <tbody>
               @foreach($video as $mark)
                <tr>
                  
                  <td>{{$mark->title}}
                  </td>
                  
                  <td><a target="_blank" href="{{$mark->link}}">{{$mark->link}}</a></td>
                 
                </tr>

                @endforeach()
                </tbody>
               
                
              
            
                </tfoot>
              </table>
            </div>
            @endif
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

         
  

@endsection