@extends('layouts.student')
@section('content')
<br>



       @foreach($subjects as $sub)
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-star-o"></i></span>

            <div class="info-box-content">
              <!-- <span class=""><h5>{{$sub->name}}</h5></span>
               -->
               <h2>{{$sub->name}}</h2>
              <span class="info-box-number"></span>
              <h6><a href="assignmentstudent/{{$sub->id}}">Assignments</a></h6>
              <h6><a href="/marksofsubject/{{$sub->id}}">Marks</a></h6>
              <h6><a href="/pdfofsubject/{{$sub->id}}">Pdf Materials</a></h6>
              <h6><a href="videosofsubject/{{$sub->id}}">Video Materials</a></h6>
              <h6><a href="live/{{$sub->id}}">Live Video</a></h6>

            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        @endforeach()
      
@endsection