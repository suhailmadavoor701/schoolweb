@extends('layouts.studenttable')
@section('content')
<style>
.videoWrapper {
  overflow: hidden;
}
@media (min-width: 1500px) {
  .videoWrapper {
    margin-top: -135px;
  }
}
@media (min-width: 1200px) {
  .videoWrapper {
    margin-top: -75px;
  }
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Live Section
        <!-- <small>advanced tables</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

         @if($count<1)
                    
                    <div >
                       <h6>No Section found</h6>
                    </div>
                    
            @else
        
        <!-- <a href="assignement/create"><button class="open-button" onclick="">Add New Assignment</button></a> -->
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Videos</h3>
            </div>
            <!-- /.box-header -->
           <div class="videowrapper">
           
           <iframe width="560" height="315" src="https://www.youtube.com/embed/{{$video->link}}" title="YouTube 
                  video player" frameborder="0" showinfo="0" allow="accelerometer; autoplay;
                   clipboard-write; encrypted-media; 
                  gyroscope; picture-in-picture" allowfullscreen></iframe></div>
            
            @endif
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

         
  

@endsection