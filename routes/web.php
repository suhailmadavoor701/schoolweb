<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Banner;
use App\Course;
use App\Enquiry;
use Illuminate\Support\Facades\DB;
use App\Events;

use App\Http\Controllers\Controller;
use App\Standard1;
use App\Webnotifications;
use App\Student;
use App\Teacher;
use Illuminate\Routing\Route as RoutingRoute;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('paymentform','paymentController@form');
Route::get('succes','paymentController@succes');

Route::post('payment', 'paymentController@payment');
Route::post('pay', 'paymentController@pay');




Route::get('/','userController@index');
Route::get('/contact','usercontroller@contact');
Route::get('/usergallery','userController@gallery');
Route::get('/admin11', function () {

    //    return "aaa";
    $events = Events::count();
    $students = Student::count();
    $noti = Webnotifications::count();
    $enquiry = Enquiry::count();
    $course = Course::count();
    $std = Standard1::count();
    $teacher = Teacher::count();

        return view('admin.index',compact('events','students','noti','enquiry','course','teacher','std'));
    });


Auth::routes();



Route::get('/studentdashboard', function () {

    $id= Auth::guard('student')->user()->id;


    $student = DB::table('students')
->join('standard1s', 'standard1s.id', '=','students.standard_id')
->select('students.*', 'standard1s.standard as class')
->where('students.id',$id)
->first();
//    dd($student);
return view('layouts.student',compact('student'));

        // return view('admin.index',compact('events','students','noti','enquiry','course','teacher','std'));
    });

    Route::get('/teacherdashboard', function () {
        return redirect()->intended('/teacherdashborad');
});

// Route::view('/home', 'home')->middleware('auth');
//     Route::view('/admin', 'admin');
//     Route::view('/writer', 'writer');


//admin login
Route::get('/adminlogin', 'Auth\LoginController@showAdminLoginForm');
Route::post('/register/admin', 'Auth\RegisterController@createAdmin');

Route::resource('course','courseController');
Route::resource('says','sayController');
Route::resource('events','eventsController');
Route::resource('webnotifications','webnotificationsController');
Route::resource('gallery','galleryController');
Route::resource('enquiry','enquiryController');
Route::get('universitynotification','universtityController@universitynotification');



// Route::get('/teacher', 'teacherloginController@showTeacherLoginForm');
Route::get('/register/admin', 'Auth\RegisterController@showAdminRegisterForm');
Route::get('/register/writer', 'Auth\RegisterController@showWriterRegisterForm');

Route::post('/login/admin', 'Auth\LoginController@adminLogin');
Route::post('/login/writer', 'Auth\LoginController@writerLogin');
Route::post('/register/admin', 'Auth\RegisterController@createAdmin');
Route::post('/register/teacher', 'Auth\RegisterController@createTeacher');

//register teacher and login
Route::get('/teacher', 'teacherloginController@showTeacherLoginForm');
Route::post('login/teacher', 'teacherloginController@teacherlogin');
Route::get('/register/teacher', 'Auth\RegisterController@showTeacherRegisterForm');

Route::get('/register/teacher', 'Auth\LoginController@showLoginForm');
Route::get('/register/teacher', 'Auth\RegisterController@showTeacherRegisterForm');
Route::post('/register/teacher', 'Auth\RegisterController@createTeacher');

//teacherside

Route::resource('subjects','subjectController');
Route::resource('standard','standardController');
Route::resource('teacherindex','teacheraddController');
Route::resource('notifications','notificationController');
Route::resource('assignment','assignmentController');
Route::resource('pmaterials','pmaterialController');
Route::resource('videos','videoController');
Route::resource('livevideos','liveController');




Route::get('payedlist','paymentController@payments');

Route::get('submitedlist','assignmentController@list');
Route::get('submitedlist','assignmentController@list');


Route::get('/teacherdashborad', 'teacherdashboardController@index');
Route::get('/teacherlist', 'teacherdashboardController@teacherlist');
Route::get('/studentlist', 'teacherdashboardController@studentlist');
Route::get('/status/{id}', 'teacherdashboardController@status');

//marks adding and show in teacher
Route::resource('marks','markController');
Route::post('markscopy','markcopyController@store');

// pdf upload as image from here
Route::post('pdf/','PDFController@generatePDF');
Route::get('pdfadd/','PDFController@pdf');

Route::get('/pdfdelete/{id}','studentviewController@delete');


Route::get('/studentregistration', 'studentregisterController@registration');
Route::post('/registerstudent','studentregisterController@register');
Route::get('/studentloginform', 'studentregisterController@loginfrom');
Route::post('/studentlogin','studentregisterController@login');


//Student
Route::get('/studentclassmate', 'classmateController@classmates');
Route::get('/teachersclass', 'classmateController@teachers');

Route::get('/subjectbystudent', 'classmateController@subjects');
Route::get('/assignmentstudent/{id}', 'studentviewController@assignments');
Route::get('/marksofsubject/{id}', 'studentviewController@marksofsubject');
Route::get('/pdfofsubject/{id}', 'studentviewController@pdfofsubject');
Route::get('/videosofsubject/{id}', 'studentviewController@videosofsubject');
Route::get('/live/{id}', 'studentviewController@live');
Route::get('/studentsnotification', 'classmateController@notification');




Route::get('university','universtityController@university');

