<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    protected $table = 'assignments';

    protected $primaryKey = 'id';
    
    protected $fillable = ['date','topic','subject_id','standard_id'];



    public function images(){
    	
        return $this->belongsTo('App\Subject','id');
    }

}
