<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Webnotifications extends Model
{
    protected $fillable = ['section','description'];
}
