<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Teacher extends Authenticatable
{
    use Notifiable;

    protected $guard = 'teacher';

    protected $fillable = [
        'mobile_no','name', 'email', 'password','standard_id','qualification',
    ]; 

    protected $hidden = [
        'password', 'remember_token',
    ];


    protected $table = 'teacher1s';

    protected $primaryKey = 'id';

    public function images(){
    	
        return $this->belongsTo('App\Standard1','id');
    }

}
