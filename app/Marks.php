<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marks extends Model
{
    
    
    protected $table = 'marks';

    protected $primaryKey = 'id';
    
    protected $fillable = ['no','mark','subject_id'];



    public function images(){
    	
        return $this->belongsTo('App\Subject','id');
    }

   
}
