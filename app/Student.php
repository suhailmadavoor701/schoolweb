<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Student extends Authenticatable
{
    use Notifiable;

    protected $guard = 'student';

    protected $fillable = [
        'mobile_no','name','status', 'email', 'password','standard_id','roll_no',
    ]; 

    protected $hidden = [
        'password', 'remember_token',
    ];


    protected $table = 'students';

    protected $primaryKey = 'id';

    public function images(){
    	
        return $this->belongsTo('App\Standard1','id');
    }

}
