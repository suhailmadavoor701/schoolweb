<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'notifications';

    protected $primaryKey = 'id';
    
    protected $fillable = ['name','description','standard_id'];



    public function images(){
    	
        return $this->belongsTo('App\standard1','id');
    }
}
