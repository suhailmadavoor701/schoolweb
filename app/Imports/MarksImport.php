<?php

namespace App\Imports;

use App\Marks;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class MarksImport implements ToModel,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {

        // dd($row);
        return new Marks([
            'no'     => $row['no'],
           'mark'    => $row['mark'], 
           'subject_id'=>1,
        ]);
        
    }
}
