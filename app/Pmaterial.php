<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pmaterial extends Model
{
    protected $table = 'pmaterials';

    protected $primaryKey = 'id';
    
    protected $fillable = ['file','title','subject_id','standard_id'];



    public function images(){
    	
        return $this->belongsTo('App\Subject','id');
    }
}
