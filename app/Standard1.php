<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Standard1 extends Model
{
    protected $table='standard1s';
    protected $primaryKey = 'id';
    protected $fillable=['standard'];

   public function photos()
   {
       
    return $this->hasMany('standard1s','id');
    
      }
      
      public function subjects()
      {
          
       return $this->hasMany('subjects','id');
       
         }

         public function notifications()
      {
          
       return $this->hasMany('notifications','id');
       
         }  


         public function students()
         {
             
          return $this->hasMany('students','id');
          
            }  

         
}
