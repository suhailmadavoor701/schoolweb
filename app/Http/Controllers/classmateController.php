<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;

class classmateController extends Controller
{
    public function classmates()
    {
        //  return "aa";
        $standard_id= Auth::guard('student')->user()->standard_id;
        // dd($standard_id);
       $students=DB::table('students')
       ->where('standard_id',$standard_id)
       ->orderBy('roll_no','asc')
        ->get();
        // dd($students);
        $id= Auth::guard('student')->user()->id;
        $student = DB::table('students')
        ->join('standard1s', 'standard1s.id', '=','students.standard_id')
        ->select('students.*', 'standard1s.standard as class')
        ->where('students.id',$id)
        ->first();


        

        return view('student.classmates',compact('students','student'));
    }

    
    public function teachers()
    {
        $id= Auth::guard('student')->user()->id;
        $student = DB::table('students')
        ->join('standard1s', 'standard1s.id', '=','students.standard_id')
        ->select('students.*', 'standard1s.standard as class')
        ->where('students.id',$id)
        ->first();

        $standard_id= Auth::guard('student')->user()->standard_id;
        // dd($standard_id);
       $teachers=DB::table('teacher1s')
       ->where('standard_id',$standard_id)
        ->get();
        // dd($students);
        return view('student.teachers',compact('teachers','student'));
    }
    public function subjects()
    {
        $id= Auth::guard('student')->user()->id;
        $student = DB::table('students')
        ->join('standard1s', 'standard1s.id', '=','students.standard_id')
        ->select('students.*', 'standard1s.standard as class')
        ->where('students.id',$id)
        ->first();

        
        $standard_id= Auth::guard('student')->user()->standard_id;
        // dd($standard_id);
       $subjects=DB::table('subjects')
       ->where('standard_id',$standard_id)
        ->get();
        // dd($students);
        return view('student.subjects',compact('subjects','student'));
    }


    public function notification()
    {
        $id= Auth::guard('student')->user()->id;
        $student = DB::table('students')
        ->join('standard1s', 'standard1s.id', '=','students.standard_id')
        ->select('students.*', 'standard1s.standard as class')
        ->where('students.id',$id)
        ->first();

        
        $standard_id= Auth::guard('student')->user()->standard_id;
        // dd($standard_id);
       $notifications=DB::table('notifications')
       ->where('standard_id',$standard_id)
        ->get();
        // dd($students);
        return view('student.notifications',compact('notifications','student'));
    }
}
