<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Subject;

class subjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        
        $standard_id= Auth::guard('teacher')->user()->standard_id;
        // dd($standard_id);
       $subjects=DB::table('subjects')
       ->where('standard_id',$standard_id)
        ->get();

            
        // $subjects=DB::select('select * from subjects order by id DESC');
        return view('teacher.subject.index',compact('subjects'));
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {    
        return view('subjects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $standard_id= Auth::guard('teacher')->user()->standard_id;
        $sub=new Subject();
        
        $sub->name= $request->name;
        $sub->standard_id=$standard_id;
        $sub->save();
        return redirect()->back()->with('status','Successfully Uploaded Subject.Thank you');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sub=Subject::find($id);
        $sub->delete();
        return redirect()->back()->with('status','Successfully Deleted Subject.Thank you');
        }
}
