<?php

namespace App\Http\Controllers;

use App\Notification;
use App\Standard1;
use App\Teacher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;


class notificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          
        $standard_id= Auth::guard('teacher')->user()->standard_id;
        // dd($standard_id);
       $notifications=DB::table('notifications')
       ->where('standard_id',$standard_id)
        ->get();

            
        // $subjects=DB::select('select * from subjects order by id DESC');
        return view('teacher.notifications.index',compact('notifications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('teacher.notifications.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $standard_id= Auth::guard('teacher')->user()->standard_id;
        $sub=new Notification();
        
        $sub->name= $request->name;
        $sub->description= $request->description;
        $sub->standard_id=$standard_id;
        $sub->save();
        return redirect()->back()->with('status','Successfully Uploaded Notification.Thank you');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sub=Notification::find($id);
        $sub->delete();
        return redirect()->back()->with('status','Successfully Deleted Notfication.Thank you');
        
    }
}
