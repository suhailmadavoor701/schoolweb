<?php

namespace App\Http\Controllers;

use App\Course;
use Illuminate\Http\Request;
use DB;
use Validator;
use File;

class courseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $course=DB::select('select * from courses order by id DESC');

    return view('admin.course.index',compact('course'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:25',
            'description' => 'required|max:100',
           
        ]);
    
    
    
    
          if ($validator->fails()) {
                 return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
            }
            
    
                
                $image =new Course();
                
                if ($request->hasFile('file')) {
                    $dir = 'uploads/course';
                    $extension = strtolower($request->file('file')->getClientOriginalExtension()); // get image extension
                    $fileName = str_random() . '.' . $extension; // rename image
                    $request->file('file')->move($dir, $fileName);
                    $image->image = $fileName;
                }
    
    
                // return "aaaa";
                $image->name= $request->name;
                $image->description= $request->description;

    
        
                $image->save();
                // dd($image);
            return redirect('/course')->with('status','Successfully Uploaded .Thank you');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $turf = Course::find($id);
        $image_path= public_path('uploads/course/'.$turf->image);
        if (File::exists($image_path)) {
           unlink($image_path);
       }
      $turf->delete();
      return redirect()->back()->with('status','Successfully Deleted.Thank you');
   }
}
