<?php

namespace App\Http\Controllers;

use App\Live;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Pmaterial;
use Illuminate\Support\Facades\DB;
use App\Subject;
use Validator;
use App\Video;

class liveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $standard= Auth::guard('teacher')->user()->standard_id;

        $livevideos = DB::table('lives')
            ->join('subjects', 'subjects.id', '=', 'lives.subject_id')
            ->select('lives.*', 'subjects.name')
            ->where('lives.standard_id','=',$standard)
            ->get();
            // dd($livevideos);

        return view('teacher.live.index',compact('livevideos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $items = Subject::all(['id', 'name']);
        $table = Subject::orderBy('id','DESC')->get();

        return view('teacher.live.create',compact('items',$items,'table'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $standard= Auth::guard('teacher')->user()->standard_id;

        $validator = Validator::make($request->all(), [
            'title' => 'required|max:100',
            'link' => 'required',
            'subject_id' => 'required',

        ]);




          if ($validator->fails()) {
                 return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
            }



                $image =new Live();


                // return "aaaa";
                $image->title= $request->title;
                $image->link= $request->link;
                $image->standard_id= $standard;
                $image->subject_id= $request->subject_id;


                $image->save();
                // dd($image);
            return redirect('/livevideos')->with('status','Successfully Uploaded .Thank you');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sub=Live::find($id);
        $sub->delete();
        return redirect()->back()->with('status','Successfully Deleted .Thank you');
    }
}
