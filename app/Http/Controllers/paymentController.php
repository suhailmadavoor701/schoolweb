<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Payment;
use Monolog\SignalHandler;
use Razorpay\Api\Api;
use Razorpay\Api\Errors\SignatureVerificationError;
use Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class paymentController extends Controller
{

    // rzp_test_vWAymUetNf6ep0
    // MlI9D8kSLD96SeC4FwDrPUpP

    public function form()
    {
        return view('school.paymentform');
    }

    public function succes()
    {

        return view('school.succes');
    }

    public function payment(Request $request)
    {

        $class = $request->input('class');
        $no = $request->input('no');
        $section= $request->input('section');
        $name = $request->input('name');
        $amount = $request->input('amount');

        $api = new Api('rzp_test_NjLf3nMNeudyaD','uUtEVlu4MzmSsYPRpI80PzGW');
        $order  = $api->order->create(array('receipt' => '123', 'amount' => $amount * 100 , 'currency' => 'INR')); // Creates order
        $orderId = $order['id'];

        $user_pay = new Payment();

        $user_pay->name = $name;
        $user_pay->amount = $amount;
        $user_pay->section = $section;
        $user_pay->no = $no;
        $user_pay->class = $class;
        $user_pay->payment_id = $orderId;
        $user_pay->save();

        $data = array(
            'order_id' => $orderId,
            'amount' => $amount
        );

        // Session::put('order_id', $orderId);
        // Session::put('amount' , $amount);

        //  return "aa";
        return redirect('/paymentform')->with('data', $data);
    }


    public function pay(Request $request){

        $data = $request->all();

        $user = Payment::where('payment_id', $data['razorpay_order_id'])->first();
        $user->payment_done = true;
        $user->razorpay_id = $data['razorpay_payment_id'];
        $user->save();
       return redirect('/succes');
}


public function payments()
{
    $standard= Auth::guard('teacher')->user()->standard_id;



      $cls = DB::table('standard1s')
    ->where('id', '=',$standard)
    ->first();
    // return ;

    $list= DB::table('payments')
    ->where('class', '=',$cls->standard)
    ->get();
    return view('teacher.paymentlist',compact('list'));



}
}
