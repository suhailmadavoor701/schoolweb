<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\MarksImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Marks;
use App\Subject;

class markController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
       $marks = DB::table('marks')
       ->join('subjects', 'subjects.id', '=', 'marks.subject_id')
       ->select('marks.*', 'subjects.name')
       ->orderBy('no', 'asc')
       ->orderBy('subject_id', 'asc')
       ->get();

       


        return view('teacher.marks.index',compact('marks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $items = Subject::all(['id', 'name']);
        $table = Subject::orderBy('id','DESC')->get();
   
        return view('teacher.marks.create',compact('items',$items,'table'));
        // return view('teacher.marks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file=$request->file('image');
        // dd($file);
        $marksArr = $this->csvToArray($request->image);
        
        foreach($marksArr as $marks)
        {
            //two
            $marks = array_merge($marks, [
                'subject_id' => $request->subject_id
            ]);
            // dd($marks);
            Marks::create($marks);
            // //one
            // $mark->update([
            //     'subject_id' => $request->subject_id
            // ]);
        }
        
        
        return redirect()->back()->with('status','Successfully Uploaded Marks.Thank you');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        
        $marks=Marks::find($id);
        // dd($marks);
                return view('teacher.marks.edit',compact('marks','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'marks' => 'required|max:10',
           
        ]);
    
    
    
    
          if ($validator->fails()) {
                 return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
            }
        
    
                
                $image = Marks::find($id);
                $image->mark= $request->marks;
                $image->save();
                // dd($image);
            return redirect('marks')->with('status','Successfully Updated .Thank you');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
            {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        return $data;
    }
}
