<?php

namespace App\Http\Controllers;

use App\Events;
use Illuminate\Http\Request;
use DB;
use Validator;
use File;
use App\Gallery;

class galleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gallery=DB::select('select * from galleries order by id DESC');
        $count = Gallery::count();

        return view('admin.gallery.index',compact('gallery','count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        
      
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:10',
            'image' => 'required',
        ]);
    
          if ($validator->fails()) {
                return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
            }
        
    
               if ($request->hasFile('image')){
    
                $images_array = $request->file('image');
                $array_len = count($images_array);
    
                for ($i=0; $i<$array_len; $i++){
                    $extension = $images_array[$i]->extension();
                    $filename = rand(123456,999999).'.'.$extension;
                    $path = public_path('uploads/gallery');
                    $images_array[$i]->move($path,$filename);
    
                    $table = new Gallery();
                    $table->name = $request->name;
                   
    
                    $table->image  = $filename;
    
                 $table->save();
    
             }
                    
    
    
            return redirect()->back()->with('status','Successfully Uploaded .Thank you');
    
         }
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $turf =Gallery::find($id);
        $image_path= public_path('uploads/gallery/'.$turf->image);
        if (File::exists($image_path)) {
           unlink($image_path);
       }
      $turf->delete();
      return redirect()->back()->with('status','Successfully Deleted.Thank you');
   }
}
