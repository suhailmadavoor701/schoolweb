<?php

namespace App\Http\Controllers;

use App\Assignment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Subject;
use App\Ass;
use File;


class studentviewController extends Controller
{
    
    public function assignments($id)
    {

        $id1= Auth::guard('student')->user()->id;
        $student = DB::table('students')
        ->join('standard1s', 'standard1s.id', '=','students.standard_id')
        ->select('students.*', 'standard1s.standard as class')
        ->where('students.id',$id1)
        ->first();
        $id1= Auth::guard('student')->user()->id;
        $ass = DB::table('assignments')
        ->join('asses', 'asses.topic', '=', 'assignments.topic')
        ->where('no',3)
        ->count();
        // dd($ass);


        $final= DB::table('assignments')
        ->join('asses', 'asses.ass_id', '=', 'assignments.id')
        ->where('no',3)
        ->count();


        

        $subject=Subject::find($id);

        $assignments = DB::table('assignments')
        ->where('subject_id', '=',$subject->id)
        ->get();

            //  dd($assignments);

       
        $subject_id=$subject->id;

        $no= Auth::guard('student')->user()->roll_no;

        // $ass = DB::table('asses')
        // ->join('assignments', 'asses.subject', '=', 'assignments.subject_id')
        // ->select('asses.*','assignments.*')
        // ->get();
        $ass = DB::table('asses')
        ->where('subject', '=',$subject->id)
        ->where('no',$no)
        ->get();
        // 
        
        // $test=DB::select('select * from assignments where id not in select *
        // ass_id from asses');
        $subject=Subject::find($id);
        $subject_id=$subject_id;
        $test1=DB::table('assignments')->select('*')->where('subject_id','=',$subject_id)
        
        ->whereNotIn('id',function($query) {

            $query->select('ass_id')->from('asses');
         
         })->get();


         $test=DB::table('assignments')
            ->join('asses', 'assignments.id', '=', 'asses.ass_id')
            ->get();




        // dd($test1);
        

        return view('student.assignment',compact('assignments','student','ass','subject','test1'));
    }


    public function marksofsubject($id)
    {

        $id1= Auth::guard('student')->user()->id;
        $no= Auth::guard('student')->user()->roll_no;
        // dd($no);


        $student = DB::table('students')
        ->join('standard1s', 'standard1s.id', '=','students.standard_id')
        ->select('students.*', 'standard1s.standard as class')
        ->where('students.id',$id1)
        ->first();

        $subject=Subject::find($id);
        // dd($subject);
        $mark= DB::table('marks')
        ->join('subjects','subjects.id','=','marks.subject_id')
        ->select('marks.*', 'subjects.name as subject')
        ->where('subject_id', '=',$subject->id)
        ->where('no',$no)
        ->get();

        $count= DB::table('marks')
        ->join('subjects','subjects.id','=','marks.subject_id')
        ->select('marks.*', 'subjects.name as subject')
        ->where('subject_id', '=',$subject->id)
        ->where('no',$no)
        ->count();

        // dd($count);

        return view('student.marks',compact('mark','student','count'));
    }
    public function pdfofsubject($id)
    {


        $id1= Auth::guard('student')->user()->id;
        $no= Auth::guard('student')->user()->roll_no;
        // dd($no);
        $student = DB::table('students')
        ->join('standard1s', 'standard1s.id', '=','students.standard_id')
        ->select('students.*', 'standard1s.standard as class')
        ->where('students.id',$id1)
        ->first();

        $subject=Subject::find($id);
        // dd($subject);
        $pdf= DB::table('pmaterials')
        ->join('subjects','subjects.id','=','pmaterials.subject_id')
        ->select('pmaterials.*', 'subjects.name as subject')
        ->where('subject_id', '=',$subject->id)
        ->get();
        $count= DB::table('pmaterials')
        ->join('subjects','subjects.id','=','pmaterials.subject_id')
        ->select('pmaterials.*', 'subjects.name as subject')
        ->where('subject_id', '=',$subject->id)
        ->count();

        // dd($count);

        return view('student.pdf',compact('pdf','student','count'));
    }


    public function videosofsubject($id)
    {


        $id1= Auth::guard('student')->user()->id;
        $no= Auth::guard('student')->user()->roll_no;
        // dd($no);
        $student = DB::table('students')
        ->join('standard1s', 'standard1s.id', '=','students.standard_id')
        ->select('students.*', 'standard1s.standard as class')
        ->where('students.id',$id1)
        ->first();

        $subject=Subject::find($id);
        // dd($subject);
        $video= DB::table('videos')
        ->join('subjects','subjects.id','=','videos.subject_id')
        ->select('videos.*', 'subjects.name as subject')
        ->where('subject_id', '=',$subject->id)
        ->get();
        $count= DB::table('videos')
        ->join('subjects','subjects.id','=','videos.subject_id')
        ->select('videos.*', 'subjects.name as subject')
        ->where('subject_id', '=',$subject->id)
        ->count();

        // dd($count);

        return view('student.videos',compact('video','student','count'));
    }
   
    public function delete($id)
    {

        $turf =Ass::find($id);
        $image_path= public_path('/uploads/pdf/'.$turf->pdf);
        if (File::exists($image_path)) {
           unlink($image_path);
       }
      $turf->delete();
      return redirect()->back()->with('status','Successfully Deleted.Thank you');

        
        
    }





    
     
    public function live($id)
    {


        $id1= Auth::guard('student')->user()->id;
        $no= Auth::guard('student')->user()->roll_no;
        // dd($no);
        $student = DB::table('students')
        ->join('standard1s', 'standard1s.id', '=','students.standard_id')
        ->select('students.*', 'standard1s.standard as class')
        ->where('students.id',$id1)
        ->first();

        $subject=Subject::find($id);
        // dd($subject);
        $video= DB::table('lives')
        ->join('subjects','subjects.id','=','lives.subject_id')
        ->select('lives.*', 'subjects.name as subject')
        ->where('subject_id', '=',$subject->id)
        ->orderby('id','desc')
        ->first();
        $count= DB::table('lives')
        ->join('subjects','subjects.id','=','lives.subject_id')
        ->select('lives.*', 'subjects.name as subject')
        ->where('subject_id', '=',$subject->id)
        ->count();

        // dd($count);

        return view('student.live',compact('video','student','count'));
    }
   



}
