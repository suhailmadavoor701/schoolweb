<?php

namespace App\Http\Controllers;

use App\Webnotifications;
use Illuminate\Http\Request;
use DB;
use Validator;
use File;

class webnotificationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $webnoti=DB::select('select * from webnotifications order by id DESC');

    return view('admin.webnoti.index',compact('webnoti'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:25',
            'description' => 'required|max:200',
       
           
        ]);
    
    
    
    
          if ($validator->fails()) {
                 return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
            }
            
    
                
                $image =new Webnotifications();
                $image->section= $request->name;
                $image->description= $request->description;
                
                 $image->save();
                // dd($image);
            return redirect('/webnotifications')->with('status','Successfully Uploaded .Thank you');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $turf = Webnotifications::find($id);
      $turf->delete();
      return redirect()->back()->with('status','Successfully Deleted.Thank you');
   }
}
