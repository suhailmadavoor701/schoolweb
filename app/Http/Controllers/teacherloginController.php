<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Config;

class teacherloginController extends Controller
{
    public function showTeacherLoginForm()
    {
        // return view('auth.login', [
        //     'url' => Config::get('constants.guards.teacher')
        // ]);
        return view('auth.teacherlogin');
    }

    public function teacherlogin(Request $request){


             

        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('teacher')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {
             
            return redirect()->intended('/teacherdashborad');
        }
        return back()->withInput($request->only('email', 'remember'));
    }
    



}
