<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Category;
use App\Blog;
use App\Images1;
use App\Images;
use App\Album1;
use App\Album;
use App\Test;
use App\Team;
class indexcontroller extends Controller
{
    public function index()
    {
         
         return view('banner.create');

    }

     public function blog()
    {

    	$category=DB::select('select * from categories order by id DESC');
         $blog=DB::select('select * from blogs order by id DESC');
        return view('users.blog',compact('blog','category'));
         
         

    }

    public function show1($id)
    {
    	$cate=DB::select('select * from categories order by id DESC');

    	$category = Category::find($id);
        $blog = Blog::orderBy('id','DESC')->where('categories_id',$id)->get();
        $count = Blog::where('categories_id',$id)->count();
        // dd($count);
        return view('users.blog1',compact('category','blog','count','cate'));
    }




     public function showsingle($id)
    {
        $cate=DB::select('select * from categories order by id DESC');

        $category = Blog::find($id);
        $blog = Blog::orderBy('id','DESC')->where('categories_id',$id)->get();
       
        $count = Blog::where('categories_id',$id)->count();
        // dd($count);
        return view('users.blogsingle',compact('category','blog','count','cate'));
    }


      public function comercial()
    {
        $banner=DB::select('select * from album1s order by id DESC');
        // dd($banner);
        return view('users.comercial',compact('banner'));
    
    }

      public function comshow($id)
    {
       $album = Album1::find($id);
        $table = Images1::orderBy('id','DESC')->where('album1_id',$id)->get();
        $count = Images1::where('album1_id',$id)->count();
        return view('users.comshow',compact('album','table','count'));
    
    }

      public function residentail()
    {
        $banner=DB::select('select * from albums order by id DESC');
        // dd($banner);
        return view('users.residentail',compact('banner'));
    
    }

      public function residentailshow($id)
    {
       $album = Album::find($id);
        $table = Images::orderBy('id','DESC')->where('album_id',$id)->get();
        $count = Images::where('album_id',$id)->count();
        return view('users.resshow',compact('album','table','count'));
    
    }



      public function team()
    {

        
         $blog=DB::select('select * from teams order by id DESC');
        return view('users.team',compact('blog'));
         
         

    }

      public function testi()
    {

        
         $blog=DB::select('select * from tests order by id DESC');
        return view('users.testi',compact('blog'));
         
         

    }

}
