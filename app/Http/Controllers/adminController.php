<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class adminController extends Controller
{
    public function teacherregister()
    {
        return view('auth.login', ['url' => 'teacher']);
    }

    public function adminLogin(Request $request)
    {

        
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        

        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {
             
            return  "aaa";
            return redirect()->intended('/admin');
        }
        return back()->withInput($request->only('email', 'remember'));
    }
      


    

}
