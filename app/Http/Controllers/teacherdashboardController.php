<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Student;
use Illuminate\Support\Facades\Auth;

class teacherdashboardController extends Controller
{
    public function index()
    {

        $standard_id= Auth::guard('teacher')->user()->standard_id;
        // dd($standard_id);
       $students=DB::table('students')
       ->where('standard_id',$standard_id)
       ->orderBy('roll_no','asc')
       ->get()->count();

       $notifications=DB::table('notifications')
       ->where('standard_id',$standard_id)
       ->get()->count();

       $subjects=DB::table('subjects')
       ->where('standard_id',$standard_id)
       ->get()->count();
       
        return view('teacher.index',compact('students','notifications','subjects'));
    }

    public function teacherlist()
    {
        $standard_id= Auth::guard('teacher')->user()->standard_id;
        // dd($standard_id);
       $teachers=DB::table('teacher1s')
       ->where('standard_id',$standard_id)
        ->get();
        // dd($teachers);
        return view('teacher.teachersindex',compact('teachers'));

    }

    public function studentlist()
    {
        $standard_id= Auth::guard('teacher')->user()->standard_id;
        // dd($standard_id);
       $students=DB::table('students')
       ->where('standard_id',$standard_id)
       ->orderBy('roll_no','asc')
        ->get();
        // dd($students);
        return view('teacher.student.studentlist',compact('students'));

    }

    public function status($id)
    {


       
        
        $status=Student::find($id)->status;
        $name=Student::find($id)->name;
        $standard_id=Student::find($id)->standard_id;
        $roll_no=Student::find($id)->roll_no;
        $mobile_no=Student::find($id)->mobile_no;
        $email=Student::find($id)->email;
        $password=Student::find($id)->password;
        
        
       if($status==1)
       {
        $student=Student::find($id);
        
        $student->status=0;
        $student->name=$name;
        $student->standard_id=$standard_id;
        $student->roll_no=$roll_no;
        $student->mobile_no=$mobile_no;
        $student->email=$email;
        $student->password=$password;
        $student->save();
        // dd($student);
        return back()->with('status','Updated Successfully');


         
       }

       if($status==0)
       {
        $student=Student::find($id);
        
        $student->status=1;
        $student->name=$name;
        $student->standard_id=$standard_id;
        $student->roll_no=$roll_no;
        $student->mobile_no=$mobile_no;
        $student->email=$email;
        $student->password=$password;
        $student->save();
        // dd($student);
        return back()->with('status','Updated Successfully');

       }
    }
}


