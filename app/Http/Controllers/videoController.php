<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Pmaterial;
use DB;
use App\Subject;
use Validator;
use App\Video;

class videoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $standard= Auth::guard('teacher')->user()->standard_id;

        $videos = DB::table('videos')
            ->join('subjects', 'subjects.id', '=', 'videos.subject_id')
            ->select('videos.*', 'subjects.name')
            ->where('videos.standard_id','=',$standard)
            ->get();
            // dd($assignments);

        return view('teacher.video.index',compact('videos'));
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $items = Subject::all(['id', 'name']);
        $table = Subject::orderBy('id','DESC')->get();
   
        return view('teacher.video.create',compact('items',$items,'table'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $standard= Auth::guard('teacher')->user()->standard_id;
        
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:10',
            'link' => 'required',
            'subject_id' => 'required',
           
        ]);
    
    
    
    
          if ($validator->fails()) {
                 return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
            }
            
    
                
                $image =new Video();
    
    
                // return "aaaa";
                $image->title= $request->title;
                $image->link= $request->link;
                $image->standard_id= $standard;
                $image->subject_id= $request->subject_id;
    
        
                $image->save();
                // dd($image);
            return redirect('/videos')->with('status','Successfully Uploaded .Thank you');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sub=Video::find($id);
        $sub->delete();
        return redirect()->back()->with('status','Successfully Deleted .Thank you');
    }
}
