<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\Student;
use App\Subject;
use App\Standard1;
use DB;
use Auth;
use Illuminate\Support\Facades\Input;

class studentregisterController extends Controller
{
    public function registration()
    {
        $items =Standard1::all(['id', 'standard']);
        $table =Standard1::orderBy('id','DESC')->get();
   
        return view('school.registration',compact('items',$items,'table'));
        return view('school.registration');
    }


    public function register(Request $request)
    {
        $standard_id=$request->standard_id;
        $roll_no=$request->roll_no;

        $user = DB::table('students')
        ->where('standard_id', $standard_id)
        ->where('roll_no', $roll_no)
        ->first();

        // $user = Student::where('roll_no', '=', $request->roll_no)->first();
        
         if ($user === null) {
            

            $validator = Validator::make($request->all(), [
                'name' => 'required|max:100',
                'email' => 'required',
                'password' => 'required|min:6',
                'roll_no' => 'required|max:50',
                'standard_id' => 'required|max:100',
                'mobile_no' => 'required|min:10|max:10',
                
        
        
            ]);
            
              if ($validator->fails()) {
                    return redirect('/studentregistration')
                                ->withErrors($validator)
                                ->withInput();
                }
            
               
        
              
            $teacher = Student::create([
                'name' => $request['name'],
                'email' => $request['email'],
                // 'qualification' => $request['standard'],
                'roll_no'=>$request['roll_no'],
                'standard_id' => $request['standard_id'],
                'mobile_no' => $request['mobile_no'],
                'password' => Hash::make($request['password']),
            ]);

               
            return redirect()->back()->with('status','Successfully Registered .Thank you');
                }

                else{

                    return redirect()->back()->with('danger','Student Already Exist with that Roll No. 
                    Try With Your Roll No');
                }

          
          
        

    }

    public function loginfrom()
    {

        return view('school.login');
    }


    public function login(Request $request)
    { 

        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        
        
        if (Auth::guard('student')->attempt(['email' => $request->email, 'password' => $request->password,'status' => 1], $request->get('remember'))) {
            
            $id= Auth::guard('student')->user()->id;
            

            $student = DB::table('students')
       ->join('standard1s', 'standard1s.id', '=','students.standard_id')
       ->select('students.*', 'standard1s.standard as class')
       ->where('students.id',$id)
       ->first();
    //    dd($student);
    return view('layouts.student',compact('student'));

            // return view('layouts.student')->with('status','You are Logged in as student!');
            
            // return redirect('/studentdashboard')
        }
        else{

            return redirect('/studentloginform')->with('danger','Please Take Your Aprovel!!!!!!!!!!!');
        }
    }
}
