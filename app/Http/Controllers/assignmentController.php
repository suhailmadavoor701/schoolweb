<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subject;
use App\Assignment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class assignmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


    $standard= Auth::guard('teacher')->user()->standard_id;

        $assignments = DB::table('assignments')
            ->join('subjects', 'subjects.id', '=', 'assignments.subject_id')
            ->select('assignments.*', 'subjects.name')
            ->where('assignments.standard_id','=',$standard)
            ->get();
            // dd($assignments);

        return view('teacher.assignment.index',compact('assignments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $items = Subject::all(['id', 'name']);
        $table = Subject::orderBy('id','DESC')->get();

        return view('teacher.assignment.create',compact('items',$items,'table'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $standard_id= Auth::guard('teacher')->user()->standard_id;

        $sub=new Assignment();
        $sub->topic= $request->topic;
        $sub->subject_id=$request->subject_id;
        $sub->standard_id=$standard_id;
        $sub->date=$request->date;
        $sub->save();
        return redirect()->back()->with('status','Successfully Uploaded Assignments.Thank you');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sub=Assignment::find($id);
        $sub->delete();
        return redirect()->back()->with('status','Successfully Deleted Assignment.Thank you');
    }


    public function list()
    {

        $standard= Auth::guard('teacher')->user()->standard_id;



      $cls = DB::table('standard1s')
    ->where('id', '=',$standard)
    ->first();



        $ass = DB::table('asses')
        ->join('subjects', 'subjects.id', '=', 'asses.subject')
        ->select('asses.*', 'subjects.name as sub')
        ->where('asses.class','=',$cls->standard)
        ->get();
            // dd($ass);

        return view('teacher.assignment.list',compact('ass'));
    }
}
