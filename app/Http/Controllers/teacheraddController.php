<?php

namespace App\Http\Controllers;

use App\Standard1;
use App\Teacher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;



class teacheraddController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $banner=DB::select('select * from teacher1s order by id DESC');

         $banner = DB::table('teacher1s')
            ->join('standard1s', 'standard1s.id', '=', 'teacher1s.standard_id')
            ->select('teacher1s.*', 'standard1s.standard as stdname')

            ->get();


        // dd($banner);
        return view('teacheradd.index',compact('banner'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $items = Standard1::all(['id', 'standard']);
        $table = Standard1::orderBy('id','DESC')->get();

        return view('teacheradd.create',compact('items',$items,'table'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:100',
            'email' => 'required',
            'password' => 'required|min:6',
            'qualification' => 'required|max:100',
            'phoneno' => 'required|min:10|max:10',



        ]);

          if ($validator->fails()) {
                return redirect('/teacherindex/create')
                            ->withErrors($validator)
                            ->withInput();
            }


        //   return  $n = $request['password'];
        $teacher = Teacher::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'qualification' => $request['qualification'],
            'standard_id' => $request['standard'],
            'mobile_no' => $request['phoneno'],
            'password' => Hash::make($request['password']),
        ]);

        return redirect('/teacherindex')->with('status','Successfully Uploaded Teacher .Thank you');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sub=Teacher::find($id);
        $sub->delete();
        return redirect()->back()->with('status','Successfully Deleted .Thank you');
    }
}
