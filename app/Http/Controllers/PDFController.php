<?php
namespace App\Http\Controllers;
  
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use PDF;
use Auth;
use App\Ass;
use App\Assignment;
use App\Subject;
use App\Standard1;


class PDFController extends Controller
{
   
    public function pdf()
    {

        $id= Auth::guard('student')->user()->id;
        $student = DB::table('students')
        ->join('standard1s', 'standard1s.id', '=','students.standard_id')
        ->select('students.*', 'standard1s.standard as class')
        ->where('students.id',$id)
        ->first();
        // $subject=Subject::find($id);
        // dd($subject);
        $ass=DB::select('select * from asses order by id DESC');
        // dd($ass);

        $items = Subject::all(['id', 'name']);
        $table = Subject::orderBy('id','DESC')->get();

        $stands =Standard1::all(['id', 'standard']);
        $standtable =Standard1::orderBy('id','DESC')->get();


        $assignment =Assignment::all(['id', 'topic']);
        $standtable =Standard1::orderBy('id','DESC')->get();



        return view('student.pdf.create',compact('student','items','table','standtable','assignment'));
    }

 public function generatePDF(Request $request)
    {
       
        //upload image to view and show 
       
        // $standard= $request->standard_id;
        // $class = DB::table('standard1s')
        // ->where('id', '=',$standard)
        // ->first();

        $id1= Auth::guard('student')->user();
        $id=$id1->standard_id;
        $class1 = DB::table('standard1s')
        ->where('id', '=',$id)
        ->first();
        // dd($class);
        $class=$class1->standard;

        $no=$id1->roll_no;


        

        $subject_id= $request->subject_id;
        $subject1 = DB::table('subjects')
        ->where('id', '=',$subject_id)
        ->first();
        $subject=$subject1->name;
        // dd($subject);

        $topic_id= $request->topic;
        $topic= DB::table('assignments')
        ->where('id', '=',$topic_id)
        ->first();
        
        $name=$topic->topic;
        $file=$request->name;
       $ass_id=$topic->id;

       $data = [
        
        'title' =>$name ,
        'no'=> $no,
       'name' => $request->name,
       'class' => $class,
       'subject' => $subject,
       'content' => $request->content,
        
    ];
    // dd($data);
    $pdf = PDF::loadView('myPDF', $data);
 
    $path = public_path('uploads/pdf/');
    $fileName =  $file . '.' . 'pdf' ;
    $pdf->save($path . '/' . $fileName);

    // return "aaa";
    // return $pdf->download($fileName);
    // save('/path-to/my_stored_file.pdf')->stream('download.pdf');
    $image =new Ass();
                $image->name= $request->name;
                $image->class= $class;
                $image->no= $no;
                $image->subject= $request->subject_id;
                $image->topic= $name;
                $image->ass_id=$ass_id;
                
                $image->pdf=$fileName;
                $image->save();

    

    return redirect('/subjectbystudent')->with('status','Succesfully Submitted Assignment');

}
}