<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Webnotifications;
use App\Events;
use Illuminate\Support\Facades\DB; 
use App\Gallery;

class userController extends Controller
{
   
    public function index()
    {

        $course=DB::select('select * from courses order by id DESC');
        $says=DB::select('select * from says order by id DESC');
        $events=DB::select('select * from events order by id DESC');
        $count = Events::count();
        $webnoti=DB::select('select * from webnotifications order by id DESC');
        $webcount = Webnotifications::count();
          
          return view('school.index',compact('says','course','events','count','webnoti','webcount'));

    }


    public function gallery()
    {
        $gallery=DB::select('select * from galleries order by id DESC');
        $count = Gallery::count();
        return view('school.gallery',compact('gallery','count'));
    }

    public function contact()
    {

        return view('school.contact');
    }

}
