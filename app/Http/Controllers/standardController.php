<?php

namespace App\Http\Controllers;

use App\Standard;
use Illuminate\Http\Request;
use DB;
use App\Standard1;
use Validator;

class standardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banner=DB::select('select * from standard1s order by id DESC');
        return view('standard.index',compact('banner'));
        // return view('standard.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('standard.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


    $validator = Validator::make($request->all(), [
        'name' => 'required|max:10',
        
    ]);

    

            $image = new Standard1();
           
            $image->standard= $request->standard;

          
            $image->save();
        
        return redirect()->back()->with('status','Successfully Uploaded Class.Thank you');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $std=Standard1::find($id);
        $std->delete();
        return redirect()->back();
    }
}
