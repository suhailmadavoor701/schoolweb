<?php

namespace App\Http\Controllers;

use App\Events;
use Illuminate\Http\Request;
use DB;
use Validator;
use File;

class eventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events=DB::select('select * from events order by id DESC');

        return view('admin.events.index',compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:25',
            'description' => 'required|max:100',
           
        ]);
    
    
    
    
          if ($validator->fails()) {
                 return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
            }
            
    
                
                $image =new Events();
                
                if ($request->hasFile('file')) {
                    $dir = 'uploads/events';
                    $extension = strtolower($request->file('file')->getClientOriginalExtension()); // get image extension
                    $fileName = str_random() . '.' . $extension; // rename image
                    $request->file('file')->move($dir, $fileName);
                    $image->image = $fileName;
                }
    
    
                // return "aaaa";
                $image->name= $request->name;
                $image->description= $request->description;

    
        
                $image->save();
                // dd($image);
            return redirect('/events')->with('status','Successfully Uploaded .Thank you');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $turf =Events::find($id);
        $image_path= public_path('uploads/events/'.$turf->image);
        if (File::exists($image_path)) {
           unlink($image_path);
       }
      $turf->delete();
      return redirect()->back()->with('status','Successfully Deleted.Thank you');
   }
}
