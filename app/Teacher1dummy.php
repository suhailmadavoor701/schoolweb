<?php
namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Teacher1 extends Authenticatable
{
    use Notifiable;

    protected $guard = 'teacher';

    protected $table = 'teacher1s';

    protected $primaryKey = 'id';
    
    protected $fillable = ['mobile_no','name','qualification','email','password','standard_id'];

    public function images(){
    	
        return $this->belongsTo('App\standard1','id');
    }
}