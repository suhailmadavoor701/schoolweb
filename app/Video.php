<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{ 
    
    protected $table = 'videos';

    protected $primaryKey = 'id';
    
    protected $fillable = ['link','title','subject_id','standard_id'];



    public function images(){
    	
        return $this->belongsTo('App\Subject','id');
    }
}
