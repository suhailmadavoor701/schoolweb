<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    
    

    protected $table = 'subjects';

    protected $primaryKey = 'id';
    
    protected $fillable = ['id','name','standard_id'];



    public function images(){
    	
        return $this->belongsTo('App\standard1','id');
    }

    public function marks()
    {
        
     return $this->hasMany('Marks','id');
     
       }  

       public function assignments()
    {
        
     return $this->hasMany('Assignment','id');
     
       }  

       public function pmaterials()
       {
           
        return $this->hasMany('Pmaterial','id');
        
          } 
          public function videos()
          {
              
           return $this->hasMany('Video','id');
           
             }  
}
